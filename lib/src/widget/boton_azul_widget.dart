import 'package:flutter/material.dart';

class BotonAzulWidget extends StatelessWidget {
  final String text;
  final Function onPressed;

  const BotonAzulWidget({
    Key? key,
    required this.text,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: RaisedButton(
          elevation: 2,
          highlightElevation: 5,
          color: Colors.blue,
          shape: StadiumBorder(),
          onPressed: () {
            this.onPressed();
          },
          child: Container(
            width: double.infinity,
            height: 55,
            child: Center(
              child: Text(
                this.text,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 17,
                ),
              ),
            ),
          )),
    );
  }
}

class BotonWithImg extends StatelessWidget {
  final String text;
  final Function onPressed;
  final IconData icon;
  final Color color;

  const BotonWithImg({
    Key? key,
    required this.text,
    required this.onPressed,
    required this.icon,
    required this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 150,
      child: RaisedButton(
          elevation: 2,
          highlightElevation: 5,
          color: this.color,
          shape: StadiumBorder(),
          onPressed: () {
            this.onPressed();
          },
          child: Container(
            width: double.infinity,
            height: 35,
            child: Center(
              child: Text(
                this.text,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 17,
                ),
              ),
            ),
          )),
    );
  }
}
