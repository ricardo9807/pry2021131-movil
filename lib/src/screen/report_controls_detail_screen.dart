import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';
import 'package:safeapp/src/model/control_model.dart';
import 'package:safeapp/src/model/fetuses_detail_model.dart';
import 'package:safeapp/src/util/arrays_info.dart';

class ReportControlsDetailScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Jiffy.locale('es');
    final ControlModel controlArgument =
        ModalRoute.of(context)!.settings.arguments as ControlModel;

    // print('ddddddddddd ${controlModelToJson(controlArgument)}');
    print('${controlArgument.fetusesDetail!.length}');
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Detalle de control prenatal'),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
              padding: EdgeInsets.all(20.0),
              child: Column(children: [
                dato1(controlArgument),
                SizedBox(height: 15),
                dato2(controlArgument.fetusesDetail!),
                SizedBox(height: 15),
                dato3(controlArgument),
              ])),
        ),
      ),
    );
  }

  Widget dato1(ControlModel controlArgument) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5.0),
        boxShadow: <BoxShadow>[
          BoxShadow(
              color: Colors.black26,
              blurRadius: 3.0,
              offset: Offset(0.0, 5.0),
              spreadRadius: 3.0),
        ],
      ),
      child: Column(children: [
        AppBar(
          title: const Text('Datos generales', style: TextStyle(
            color: Colors.black
          )),
          automaticallyImplyLeading: false,
          backgroundColor: Colors.green[50],
        ),        
        ListTile(
          title: Text(
            '${controlArgument.actualWeight} Kg',
            style: TextStyle(fontSize: 20),
          ),
          subtitle: Text('Peso Actual'),
        ),
        ListTile(
          title: Text(
            '${controlArgument.weightGain} Kg',
            style: TextStyle(fontSize: 20),
          ),
          subtitle: Text('Ganancia de peso'),
        ),
        ListTile(
          title: Text(
            '${controlArgument.imc}',
            style: TextStyle(fontSize: 20),
          ),
          subtitle: Text('Índice de masa corporal'),
        ),
        ListTile(
          title: Text(
            '${controlArgument.temperature}°',
            style: TextStyle(fontSize: 20),
          ),
          subtitle: Text('Temperatura'),
        ),
        ListTile(
          title: Text(
            '${controlArgument.uterineHeight} cm',
            style: TextStyle(fontSize: 20),
          ),
          subtitle: Text('Altura uterinia'),
        ),
        ListTile(
          title: Text(
            '${controlArgument.systolicPressure}',
            style: TextStyle(fontSize: 20),
          ),
          subtitle: Text('Presión sistólica'),
        ),
        ListTile(
          title: Text(
            '${controlArgument.diastolicPressure}',
            style: TextStyle(fontSize: 20),
          ),
          subtitle: Text('Presión diastólica'),
        ),
        ListTile(
          title: Text(
            '${controlArgument.pulse}',
            style: TextStyle(fontSize: 20),
          ),
          subtitle: Text('Pulso'),
        ),
        ListTile(
          title: Text(
            '${controlArgument.breathingFrequency}',
            style: TextStyle(fontSize: 20),
          ),
          subtitle: Text('Frecuencia respiratoria'),
        ),
      ]),
    );
  }

  Widget dato2(List<FetusesDetailModel> data) {
    return Container(
      // height: 200,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5.0),
        boxShadow: <BoxShadow>[
          BoxShadow(
              color: Colors.black26,
              blurRadius: 3.0,
              offset: Offset(0.0, 5.0),
              spreadRadius: 3.0),
        ],
      ),
      child: Column(children: [
          AppBar(
            title: const Text('Evolución de los fetos',
                style: TextStyle(color: Colors.black)),
            automaticallyImplyLeading: false,
            backgroundColor: Colors.green[50],
          ),
          _crearListado(data),
        ])
      
      //_crearListado(data),
    );
  }

  Widget _crearListado(List<FetusesDetailModel> data) {
    // return Text('data');
    return ListView.builder(
        shrinkWrap: true,
        itemCount: data.length,
        itemBuilder: (context, i) => _crearItem(context, data[i]));
  }

  Widget _crearItem(BuildContext context, FetusesDetailModel fetus) {
    return Card(
      
        child: ListTile(
            leading: Icon(Icons.receipt),
            title: Text('Feto #${fetus.positionFetus}'),
            subtitle: Text('Ver detalle del feto'),
            onTap: () async {
              // Navigator.pushNamed(context, 'report_hcpbs_detail',
              //     arguments: hcpb);
              _mostrarAlerta(context, fetus);
            }));
  }

  void _mostrarAlerta(BuildContext context, FetusesDetailModel fetus) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return AlertDialog(
            title: Text('Datos del feto'),
            content: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  ListTile(
                    title: Text(
                      '${fetus.observation}',
                      style: TextStyle(fontSize: 18),
                    ),
                    subtitle: Text('Observación'),
                  ),
                  ListTile(
                    title: Text(
                      '${fetus.fcf}',
                      style: TextStyle(fontSize: 18),
                    ),
                    subtitle: Text('Frecuencia cardiaca fetal'),
                  ),
                  ListTile(
                    title: Text(
                      '${fetusDetail('positionFetus',fetus.positionFetus)}',
                      style: TextStyle(fontSize: 18),
                    ),
                    subtitle: Text('Posición del feto'),
                  ),
                  ListTile(
                    title: Text(
                      '${fetusDetail('situationFetus',fetus.situationFetus)}',
                      style: TextStyle(fontSize: 18),
                    ),
                    subtitle: Text('Situación del feto'),
                  ),
                  ListTile(
                    title: Text(
                      '${fetusDetail('presentationFetus',fetus.presentationFetus)}',
                      style: TextStyle(fontSize: 18),
                    ),
                    subtitle: Text('Presentación del feto'),
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              TextButton(
                  child: Text('Ok'),
                  onPressed: () => Navigator.of(context).pop())
            ],
          );
        });
  }

  Widget dato3(ControlModel controlArgument) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5.0),
        boxShadow: <BoxShadow>[
          BoxShadow(
              color: Colors.black26,
              blurRadius: 3.0,
              offset: Offset(0.0, 5.0),
              spreadRadius: 3.0),
        ],
      ),
      child: Column(children: [
        AppBar(
          title: const Text('Diagnóstico y recomendaciones', style: TextStyle(
            color: Colors.black,
            fontSize: 16,
          )),
          automaticallyImplyLeading: false,
          backgroundColor: Colors.green[50],
        ), 
        ListTile(
          title: Text(
            '${controlArgument.systemDiagnosis}',
            style: TextStyle(fontSize: 15),
          ),
          subtitle: Text('Diagnóstico del sistema'),
        ),
        ListTile(
          title: Text(
            '${controlArgument.nameSpecialist}',
            style: TextStyle(fontSize: 15),
          ),
          subtitle: Text('Especialista Encargado'),
        ),
        ListTile(
          title: Text(
            '${controlArgument.finalDiagnosis}',
            style: TextStyle(fontSize: 15),
          ),
          subtitle: Text('Diagnóstico del especialista'),
        ),
        ListTile(
          title: Text(
            '${controlArgument.recommendations}',
            style: TextStyle(fontSize: 15),
          ),
          subtitle: Text('Recomendaciones'),
        )
      ]),
    );
  }
}
