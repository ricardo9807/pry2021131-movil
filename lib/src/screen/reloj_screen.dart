import 'dart:io';

import 'package:flutter/material.dart';
import 'package:googleapis/fitness/v1.dart';
import 'package:googleapis/youtube/v3.dart';
import 'package:health/health.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:extension_google_sign_in_as_googleapis_auth/extension_google_sign_in_as_googleapis_auth.dart';

GoogleSignIn _googleSignIn = GoogleSignIn(
    clientId:
        '158790865522-4jvi1iumoeqp7nin34kd22t3n6h8bbif.apps.googleusercontent.com',
    scopes: ['email', 'https://www.googleapis.com/auth/contacts.readonly']);

// 158790865522-4jvi1iumoeqp7nin34kd22t3n6h8bbif.apps.googleusercontent.com
class RelojScreen extends StatefulWidget {
  @override
  _RelojScreenState createState() => _RelojScreenState();
}

class _RelojScreenState extends State<RelojScreen> {
  GoogleSignInAccount? _currentUser;
  String _contactText = '';

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _googleSignIn.onCurrentUserChanged.listen((GoogleSignInAccount? account) {
      setState(() {
        _currentUser = account;
      });
      if (_currentUser != null) {
        // _handleGetContact(_currentUser!);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
          appBar: AppBar(
            title: const Text('Plugin example app'),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.file_download),
                onPressed: () async {
                  // fetchData();
                  await getTest();
                },
              )
            ],
          ),
          body: Center(
            child: _buildBody(),
          )),
    );
  }

  Widget _buildBody() {
    GoogleSignInAccount? user = _currentUser;
    if (user != null) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          ListTile(
            leading: GoogleUserCircleAvatar(
              identity: user,
            ),
            title: Text(user.displayName ?? ''),
            subtitle: Text(user.email),
          ),
          const Text("Signed in successfully."),
          Text(_contactText),
          ElevatedButton(
            child: const Text('SIGN OUT'),
            onPressed: _handleSignOut,
          ),
          ElevatedButton(
            child: const Text('REFRESH'),
            onPressed: () {},
          ),
        ],
      );
    } else {
      return Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          const Text("You are not currently signed in."),
          ElevatedButton(
            child: const Text('SIGN IN'),
            onPressed: _handleSignIn,
          ),
        ],
      );
    }
  }

  Future<void> _handleSignIn() async {
    try {
      var f = await _googleSignIn.signIn();
      print('---------');
      print(f);
      print('---------');
    } catch (error) {
      print('-----ERROR-----');
      print('E: $error');
    }
  }

  Future<void> _handleSignOut() => _googleSignIn.disconnect();

  Future<void> _handleGetContact(GoogleSignInAccount user) async {
    setState(() {
      _contactText = "Loading contact info...";
    });
  }

  getTest() async {
    try {
// final auth = await GoogleSignIn.standard();
      var httpClient = (await _googleSignIn.authenticatedClient())!;
      // var g = YouTubeApi(cl);
      var f = FitnessApi(httpClient);
      // f.users.dataSources.list(userId);
      UsersDatasetResource ra = f.users.dataset;
      // UsersDataSourcesResource g = f.users.dataSources.list(userId);
      // print('f ${ra}');
      // print('f ${g.list(userId)}');
    } catch (e) {
      print('E: $e');
    }
  }
}
