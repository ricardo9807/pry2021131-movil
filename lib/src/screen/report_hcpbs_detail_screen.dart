import 'package:flutter/material.dart';
import 'package:safeapp/src/model/hcpb_model.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:jiffy/jiffy.dart';
import 'package:safeapp/src/model/medical_history_model.dart';
import 'package:safeapp/src/util/arrays_info.dart';
import 'package:safeapp/src/model/vaccination_record_model.dart';

class ReportHcpbsDetailScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Jiffy.locale('es');
    final HcpbModel hcpbArgument = ModalRoute.of(context)!.settings.arguments
        as HcpbModel; //.of(context).settings.arguments;

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('HCPB'),
      ),
      // body: datosHistorias(context, hcpbArgument.medicalHistory!),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
              padding: EdgeInsets.all(20.0),
              child: Column(children: [
                datoGeneral(hcpbArgument),
                SizedBox(height: 15),
                datoGinecologico(hcpbArgument),
                SizedBox(height: 15),
                datosHistorias(context, hcpbArgument.medicalHistory!),
                SizedBox(height: 15),
                //tableTest(context),
                //cartilla(context, hcpbArgument.vaccinationRecord!),
              ])),
        ),
      ),
      floatingActionButton: floatOpcion(context, hcpbArgument.id!),
    );
  }

  Widget floatOpcion(BuildContext context, String idHcpb) {
    return SpeedDial(
      icon: Icons.remove_red_eye,
      activeIcon: Icons.close,
      spacing: 3,
      animationSpeed: 200,
      spaceBetweenChildren: 4,
      children: [
        SpeedDialChild(
            child: Icon(Icons.assignment_turned_in_outlined),
            label: "Emergencias",
            onTap: () {
              Navigator.pushNamed(context, 'report_emergency',
                  arguments: idHcpb);
            }),
        SpeedDialChild(
            child: Icon(Icons.control_point),
            label: "Controles",
            onTap: () {
              Navigator.pushNamed(context, 'report_controls',
                  arguments: idHcpb);
            })
      ],
    );
  }

  Widget datoGinecologico(HcpbModel hcpbArgument) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5.0),
        boxShadow: <BoxShadow>[
          BoxShadow(
              color: Colors.black26,
              blurRadius: 3.0,
              offset: Offset(0.0, 5.0),
              spreadRadius: 3.0),
        ],
      ),
      child: Column(children: [
        AppBar(
          title: const Text('Datos ginecológicos',
              style: TextStyle(color: Colors.black)),
          automaticallyImplyLeading: false,
          backgroundColor: Colors.orange[50],
        ),
        ListTile(
          title: Text(
            '${hcpbArgument.ageMenarche}',
            style: TextStyle(fontSize: 20),
          ),
          subtitle: Text('Menarquía'),
        ),
        ListTile(
          title: Text(
            '${hcpbArgument.ageFirstSexualExperience}',
            style: TextStyle(fontSize: 20),
          ),
          subtitle: Text('Edad de primera relación sexual'),
        ),
        ListTile(
          title: Text(
            '${showAnticopcetivo(hcpbArgument.lastContraceptiveMethod)}',
            style: TextStyle(fontSize: 20),
          ),
          subtitle: Text('Último método anticonceptivo utilizado'),
        ),
        ListTile(
          title: Text(
            '${Jiffy(hcpbArgument.lastDateMenstruation).yMMMMd}',
            style: TextStyle(fontSize: 20),
          ),
          subtitle: Text('Fecha de última menstruación'),
        ),
      ]),
    );
  }

  Widget datoGeneral(HcpbModel hcpbArgument) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5.0),
        boxShadow: <BoxShadow>[
          BoxShadow(
              color: Colors.black26,
              blurRadius: 3.0,
              offset: Offset(0.0, 5.0),
              spreadRadius: 3.0),
        ],
      ),
      child: Column(children: [
        AppBar(
          title: const Text('Datos generales',
              style: TextStyle(color: Colors.black)),
          automaticallyImplyLeading: false,
          backgroundColor: Colors.orange[50],
        ),
        ListTile(
          title: Text(
            '${hcpbArgument.gestationalAge}',
            style: TextStyle(fontSize: 20),
          ),
          subtitle: Text('Edad gestacional'),
        ),
        ListTile(
          title: Text(
            '${hcpbArgument.currentControl}',
            style: TextStyle(fontSize: 20),
          ),
          subtitle: Text('Número de controles'),
        ),
        ListTile(
          title: Text(
            '${Jiffy(hcpbArgument.probableDateBirth).yMMMMd}',
            style: TextStyle(fontSize: 20),
          ),
          subtitle: Text('Fecha probable de parto'),
        ),
        ListTile(
          title: Text(
            '${showYesOrNot(hcpbArgument.multiplePregnancy)}',
            style: TextStyle(fontSize: 20),
          ),
          subtitle: Text('Embarazo múltiple'),
        ),
        ListTile(
          title: Text(
            '${showYesOrNot(hcpbArgument.previousPretermPregnancy)}',
            style: TextStyle(fontSize: 20),
          ),
          subtitle: Text('¿Tiene antecedentes de parto pretérmino?'),
        ),
      ]),
    );
  }

  // Widget cartilla(BuildContext context, List<VaccinationRecordModel>data  ) {
  //   return Container(
  //     decoration: BoxDecoration(
  //       color: Colors.white,
  //       borderRadius: BorderRadius.circular(5.0),
  //       boxShadow: <BoxShadow>[
  //         BoxShadow(
  //             color: Colors.black26,
  //             blurRadius: 3.0,
  //             offset: Offset(0.0, 5.0),
  //             spreadRadius: 3.0),
  //       ],
  //     ),
  //     child: Column(children: [
  //       AppBar(
  //         title: const Text('Cartilla de vacunaciones', style: TextStyle(
  //           color: Colors.black
  //         )),
  //         automaticallyImplyLeading: false,
  //         backgroundColor: Colors.orange[50],
  //       ),
  //       ListTile(
  //         title: Text(
  //           '${data[0].rubella}',
  //           style: TextStyle(fontSize: 20),
  //         ),
  //         subtitle: Text('Rubeola'),
  //       ),
  //       ListTile(
  //         title: Text(
  //           '${data[0].papilloma}',
  //           style: TextStyle(fontSize: 20),
  //         ),
  //         subtitle: Text('Papiloma'),
  //       ),
  //       ListTile(
  //         title: Text(
  //            '${data[0].ah1n1}',
  //           style: TextStyle(fontSize: 20),
  //         ),
  //         subtitle: Text('AH1N1'),
  //       ),
  //       ListTile(
  //         title: Text(
  //            '${data[0].hepatitisB}',
  //           style: TextStyle(fontSize: 20),
  //         ),
  //         subtitle: Text('Hepatitis B'),
  //       ),
  //       ListTile(
  //         title: Text(
  //           '${data[0].yellowFever}',
  //           style: TextStyle(fontSize: 20),
  //         ),
  //         subtitle: Text('Fiebre amarilla'),
  //       ),
  //       ListTile(
  //         title: Text(
  //           '${data[0].influenza}',
  //           style: TextStyle(fontSize: 20),
  //         ),
  //         subtitle: Text('Influenza'),
  //       ),
  //     ]),
  //   );
  // }

  Widget datosHistorias(BuildContext context, List<MedicalHistoryModel> data) {
    return Container(
        // height: 200,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5.0),
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.black26,
                blurRadius: 3.0,
                offset: Offset(0.0, 5.0),
                spreadRadius: 3.0),
          ],
        ),
        child: Column(children: [
          AppBar(
            title: const Text('Patologías registradas',
                style: TextStyle(color: Colors.black)),
            automaticallyImplyLeading: false,
            backgroundColor: Colors.orange[50],
          ),
          _crearListado(data),
        ]));

    // _crearListado(data);
  }

  Widget _crearListado(List<MedicalHistoryModel> data) {
    // return Text('data');
    return ListView.builder(
        shrinkWrap: true,
        itemCount: data.length,
        itemBuilder: (context, i) => _crearItem(context, data[i]));
  }

  Widget _crearItem(BuildContext context, MedicalHistoryModel history) {
    return Card(
        child: ListTile(
            leading: Icon(Icons.receipt),
            title: Text(history.description),
            subtitle: Text('Ver detalle de patología'),
            onTap: () async {
              // Navigator.pushNamed(context, 'report_hcpbs_detail',
              //     arguments: hcpb);
              _mostrarAlerta(context, history);
            }));
  }

  Widget tableTest(BuildContext context) {
    return Card(
        child: Column(children: <Widget>[
      Container(
        margin: EdgeInsets.all(10),
        child: Table(
          border: TableBorder.all(),
          children: [
            TableRow(children: [
              Column(children: [Text('My Account')]),
              Column(children: [Text('Settings')]),
              Column(children: [Text('Ideas')]),
            ]),
            TableRow(children: [
              Column(children: [Text('My Account')]),
              Column(children: [Text('Settings')]),
              Column(children: [Text('Ideas')]),
            ]),
          ],
        ),
      ),
    ]));
  }

  void _mostrarAlerta(BuildContext context, MedicalHistoryModel history) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return AlertDialog(
            title: Text('Datos'),
            content: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  ListTile(
                    title: Text(
                      '${history.description}',
                      style: TextStyle(fontSize: 18),
                    ),
                    subtitle: Text('Patología'),
                  ),
                  ListTile(
                    title: Text(
                      '${history.observation}',
                      style: TextStyle(fontSize: 18),
                    ),
                    subtitle: Text('Observación del diagnóstico'),
                  ),
                  ListTile(
                    title: Text(
                      '${Jiffy(history.diagnosisDate).yMMMMd}',
                      style: TextStyle(fontSize: 18),
                    ),
                    subtitle: Text('Fecha de diagnóstico'),
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              TextButton(
                  child: Text('Ok'),
                  onPressed: () => Navigator.of(context).pop())
            ],
          );
        });
  }
}
