import 'dart:async';

import 'package:flutter/material.dart';
import 'package:safeapp/src/util/toast_view.dart';
import 'package:flutter_blue/flutter_blue.dart';

class Test3Screen extends StatefulWidget {
  const Test3Screen({Key? key}) : super(key: key);

  @override
  _Test3ScreenState createState() => _Test3ScreenState();
}

class _Test3ScreenState extends State<Test3Screen> {
  late bool _search = false;
  late bool _encontro = false;
  //0->apagado 1->buscando 2->encendido
  int bluetoothIsActived = 0;
  final FlutterBlue flutterBlue = FlutterBlue.instance;

  @override
  void initState() {
    super.initState();

    FlutterBlue.instance.state.listen((event) {
      print('raaaaaa ${event}');

      switch (event) {
        case BluetoothState.off:
          setState(() {
            bluetoothIsActived = 0;
          });
          break;
        case BluetoothState.on:
          setState(() {
            bluetoothIsActived = 2;
          });
          break;
        default:
          setState(() {
            bluetoothIsActived = 1;
          });
          break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Sincronización '),
      ),
      body: (bluetoothIsActived == 2)
          ? cuerpoView()
          : Center(
              child: Text(
                'Es necesario activar el Bluetooth para vincular un wearable',
                textAlign: TextAlign.center,
              ),
            ),
      floatingActionButton: (bluetoothIsActived == 2) ? floatBiew() : null,
    );
  }

  Widget floatBiew() {
    return FloatingActionButton(
      child: (_search) ? Icon(Icons.stop) : Icon(Icons.search),
      onPressed: searchDevices,
      backgroundColor: (_search) ? Colors.red : Colors.blue,
    );
  }

  Widget cuerpoView() {
    if (_search) {
      return Center(
        child: Container(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [CircularProgressIndicator(), Text('Buscando...')],
        )),
      );
    } else {
      return bodyContainer();
    }
  }

  searchDevices() async {
    print('raaaaa');
    setState(() {
      _search = true;
    });
    Timer(Duration(seconds: 4), () async {
      setState(() {
        _search = false;
        _encontro = true;
      });
    });
  }

  Widget bodyContainer() {
    return (_encontro)
        ? Container(
            color: Colors.white,
            child: ListView.builder(
                itemCount: 1,
                itemBuilder: (context, i) => Card(
                    child: ListTile(
                        leading: Icon(Icons.watch),
                        title: Text('Mi band 6'),
                        subtitle: Text('42-46-46-cb-ef-fd'),
                        onTap: () async {
                          var t = await toastGod(
                              'Dispositivo conectado correctamente');
                          Timer(Duration(seconds: 2), () async {
                            Navigator.of(context).pop();
                          });

                          // print(t);
                        }))),
          )
        : Container(color: Colors.white);
  }
}
