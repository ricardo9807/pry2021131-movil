import 'package:flutter/material.dart';
import 'package:safeapp/src/widget/boton_azul_widget.dart';
import 'package:safeapp/src/widget/custom_input_widget.dart';
import 'package:safeapp/src/widget/label_widget.dart';
import 'package:safeapp/src/widget/logo_widget.dart';

class PasswordForgetScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffF2F2F2),
      body: SafeArea(
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Container(
            height: MediaQuery.of(context).size.height * 0.9,
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  LogoWidget(titulo: ''),
                  _Form(),
                  LabelWidget(
                    ruta: 'register',
                    titulo: 'No tienes cuenta?',
                    subTitulo: 'Crea una ahora!',
                  ),
                  Text(
                    'Términos y condiciones de uso',
                    style: TextStyle(fontWeight: FontWeight.w200),
                  )
                ]),
          ),
        ),
      ),
    );
  }
}

class _Form extends StatefulWidget {
  @override
  __FormState createState() => __FormState();
}

class __FormState extends State<_Form> {
  final emailCtrl = TextEditingController();
  final passCtrl = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 20),
      padding: EdgeInsets.symmetric(horizontal: 50),
      child: Column(children: <Widget>[
        CustomInputWidget(
          icon: Icons.mail_outline,
          placeholder: 'Correo',
          textController: emailCtrl,
          keyboardType: TextInputType.emailAddress,
        ),
        CustomInputWidget(
          icon: Icons.lock_outline,
          placeholder: 'Contraseña',
          textController: passCtrl,
          isPassword: true,
        ),
        CustomInputWidget(
          icon: Icons.lock_outline,
          placeholder: 'Contraseña',
          textController: passCtrl,
          isPassword: true,
        ),
        BotonAzulWidget(
          text: 'Cambiar Contraseña',
          onPressed: onClick,
        )
      ]),
    );
  }

  onClick() {
    print('pipipisssssssssssssssssssss');
    Navigator.pushReplacementNamed(context, 'home');
    // print(emailCtrl.text);
    // print(passCtrl.text);
  }
}
