import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:safeapp/src/model/control_model.dart';
import 'package:safeapp/src/providers/user_provider.dart';
import 'package:safeapp/src/util/arrays_info.dart';
import 'package:jiffy/jiffy.dart';

class ReportControlsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Jiffy.locale('es');
    final String idHcpb = ModalRoute.of(context)!.settings.arguments as String;
    final userService = Provider.of<UserProvider>(context);

    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('Mis controles'),
        ),
        body: SafeArea(
            child: Container(
                child: FutureBuilder(
          future: userService.controlsByHcpb(idHcpb),
          builder: (BuildContext context,
              AsyncSnapshot<List<ControlModel>> snapshot) {
            if (snapshot.hasData) {
              List<ControlModel> controls = [];
              controls = snapshot.data!;
              print(controlModelToJson(controls));
              return _crearListado(controls);
            }
            return Center(child: CircularProgressIndicator());
          },
        ))));
  }

  Widget _crearListado(List<ControlModel> emergencies) {
    return ListView.builder(
        itemCount: emergencies.length,
        itemBuilder: (context, i) => _crearItem(context, emergencies[i]));
  }

  Widget _crearItem(BuildContext context, ControlModel control) {
    return Card(
        child: ListTile(
            leading: Icon(Icons.calendar_today),
            title: Text('Control N°${control.numberControl}'),
            // subtitle: Text(
            //   '${showYesOrNot(control.numberControl)}',
            // ),
            subtitle: Text(
                '${Jiffy(control.dateAttention).yMMMMd}\n${control.systemDiagnosis}'),
            isThreeLine: true,
            onTap: () async {
              Navigator.pushNamed(context, 'report_controls_detail',
                  arguments: control);
            }));
  }
}
