import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:safeapp/src/model/signal_model.dart';
import 'package:safeapp/src/providers/user_provider.dart';
import 'package:safeapp/src/screen/tab_notification_screen.dart';
import 'package:safeapp/src/screen/tab_profile.dart';
import 'package:safeapp/src/screen/tab_statistics_screen.dart';
import 'package:safeapp/src/screen/tab_ubicacion_screen.dart';
import 'package:salomon_bottom_bar/salomon_bottom_bar.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with WidgetsBindingObserver {
  SignalModel signalModel = SignalModel(
      pulse: 0, bloodPresure: 0, temperature: 00.0, diastolic: 0, systolic: 0);
  int _currentIndex = 0;
  bool showMensajeAlerta = false;
  final tabs = [
    TabStatisticsScreen(),
    TabUbicacionScreen(),
    TabProfileScreen()
  ];

  @override
  void initState() {
    // TODO: implement initState
    // WidgetsBinding.instance.addObserver(this);
    super.initState();
    int tiempoBusqueda = 60 * 10;
    bool showAlerta = false;
    // UserProvider userService = Provider.of<UserProvider>(context);
    Timer.periodic(Duration(seconds: tiempoBusqueda), (timer) async {
      print('alerta es $showAlerta | $showMensajeAlerta');
      // await userService.signalModelData();
      await context.read<UserProvider>().signalModelData();
      if (showMensajeAlerta == false) {
        var p = context.read<UserProvider>().dataSignal.pulse;
        var t = context.read<UserProvider>().dataSignal.temperature;
        var s = context.read<UserProvider>().dataSignal.systolic;
        var d = context.read<UserProvider>().dataSignal.diastolic;
        print('alerta IF $showAlerta $t');
        if ((85 <= p && p <= 90) &&
            (35 <= t && t <= 37.5) &&
            (90 <= s && s <= 129) &&
            (60 <= d && d <= 89)) {
          print('estamos bien');
        } else {
          print('mostrar alerta');
          this.mensajeAlerta();
          setState(() {
            showMensajeAlerta = true;
          });
          showAlerta = true;
        }
      } else {
        print('alerta ELSE $showAlerta ');
      }
    });
    context.read<UserProvider>().iniciarSeguimiento();
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();

    // print('creando tiempo -----> $showAlerta');
    // UserProvider userService = Provider.of<UserProvider>(context);
    // Timer.periodic(Duration(seconds: tiempoBusqueda), (timer) async {
    //   // print('tiempo -----> $showAlerta');
    //   await userService.signalModelData();
    //   if (showAlerta == false) {
    //     if (userService.dataSignal.temperature > 40.0 ||
    //         userService.dataSignal.pulse > 50 ||
    //         userService.dataSignal.bloodPresure > 50) {
    //       showAlerta = true;
    //       print('tiempo if --> $showAlerta');
    //       // this.mensajeAlerta();
    //     }

    //     // this.mensajeAlerta();
    //   }
    // });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: SalomonBottomBar(
          currentIndex: _currentIndex,
          onTap: (i) => setState(() => _currentIndex = i),
          items: <SalomonBottomBarItem>[
            SalomonBottomBarItem(
              icon: Icon(Icons.analytics),
              title: Text(
                'Datos',
                style: TextStyle(color: Colors.black54),
              ),
              selectedColor: Colors.pink,
            ),
            SalomonBottomBarItem(
              icon: Icon(Icons.place),
              title: Text('Centros de salud'),
              selectedColor: Colors.pinkAccent,
            ),
            SalomonBottomBarItem(
              icon: Icon(Icons.person),
              title: Text('Perfil'),
              selectedColor: Colors.pink,
            ),
          ]),
      body: tabs[_currentIndex],
    );
  }

  void mensajeAlerta() {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (ctx) => AlertDialog(
              title: Text('signos anormales detectados'),
              actions: [
                // TextButton(
                //     child: Text('Cancelar'),
                //     onPressed: () async {
                //       setState(() {
                //         showMensajeAlerta = true;
                //       });
                //       Navigator.of(context).pop();
                //     }),
                TextButton(
                    child: Text('Ok'),
                    onPressed: () async {
                      setState(() {
                        showMensajeAlerta = false;
                      });
                      Navigator.of(context).pop();
                      Navigator.pushNamed(context, 'emergency_notify');
                    })
              ],
            ));
  }
}
