import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:safeapp/src/model/hcpb_model.dart';
import 'package:safeapp/src/providers/user_provider.dart';
import 'package:safeapp/src/util/arrays_info.dart';
import 'package:jiffy/jiffy.dart';

class ReportHcpbsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Jiffy.locale('es');
    final userService = Provider.of<UserProvider>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('Mis HCPBs'),
      ),
      backgroundColor: Color(0xffF2F2F2),
      body: SafeArea(
        child: Container(
          child: FutureBuilder(
              future: userService.hcpbsByUser(),
              builder: (BuildContext context,
                  AsyncSnapshot<List<HcpbModel>> snapshot) {
                if (snapshot.hasData) {
                  List<HcpbModel> hcpbs = [];
                  hcpbs = snapshot.data!;
                  return _crearListado(hcpbs);
                }
                return Center(child: CircularProgressIndicator());
              }),
        ),
      ),
    );
  }

  Widget _crearListado(List<HcpbModel> hcpb) {
    return ListView.builder(
        itemCount: hcpb.length,
        itemBuilder: (context, i) => _crearItem(context, hcpb[i]));
  }

  Widget _crearItem(BuildContext context, HcpbModel hcpb) {
    return Card(
        child: ListTile(
            leading: Icon(Icons.calendar_today),
            title: Text('${Jiffy(hcpb.creationDate).yMMMMd}'),
            subtitle: Text('${showHCPBStatus(hcpb.status)}'),
            onTap: () async {
              Navigator.pushNamed(context, 'report_hcpbs_detail',
                  arguments: hcpb);
            }));
  }
}
