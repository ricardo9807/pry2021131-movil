import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:safeapp/src/model/signal_model.dart';
import 'package:safeapp/src/model/values_model.dart';
import 'package:safeapp/src/providers/user_provider.dart';
import 'package:safeapp/src/util/toast_view.dart';

class EmergencyNotifyScreen extends StatefulWidget {
  @override
  _EmergencyNotifyScreenState createState() => _EmergencyNotifyScreenState();
}

class _EmergencyNotifyScreenState extends State<EmergencyNotifyScreen> {
  late bool _resp1 = false;
  late bool _resp2 = false;
  late bool _resp3 = false;
  late bool _resp4 = false;
  final _resp5 = TextEditingController();
  late bool insert = false;
  late Size size;
  late UserProvider userService;

  @override
  Widget build(BuildContext context) {
    userService = Provider.of<UserProvider>(context);
    size = MediaQuery.of(context).size;

    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('Notificación de emergencias'),
        ),
        body: SafeArea(
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: bodyContainer(),
          ),
        ),
        floatingActionButton: floatOpcion(userService));
  }

  Widget bodyContainer() {
    return FutureBuilder(
        future: userService.signalModelData2(),
        builder: (BuildContext context, AsyncSnapshot<SignalModel> snapshot) {
          if (snapshot.hasData) {
            SignalModel data = snapshot.data!;
            return Container(
              width: size.width,
              height: size.height * 0.9,
              padding: EdgeInsets.all(15.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text('Mis signos vitales'),
                  Row(
                    children: <Widget>[
                      signoVitalWidget(
                        'Presión arterial',
                        '${data.diastolic}/${data.systolic} mmhg',
                        Colors.blue[50],
                        Icons.bloodtype,
                      ),
                      signoVitalWidget(
                          'Frecuencia Cardiaca',
                          '${data.pulse} LPM',
                          Colors.amber[50],
                          Icons.favorite),
                      signoVitalWidget(
                        'Temperatura',
                        '${data.temperature}',
                        Colors.orange[50],
                        Icons.thermostat,
                      )
                    ],
                  ),
                  // Text('FORM'),
                  formWidget()
                ],
              ),
            );
          }
          return Container();
        });
  }

  Widget signoVitalWidget(
      String title, String value, Color? colorBackground, IconData icon) {
    Color? color = Colors.pink[200];
    TextStyle styleBlue = TextStyle(color: Colors.blue[200]);
    TextStyle stylePink = TextStyle(color: color);

    return Container(
        padding: EdgeInsets.all(5),
        alignment: Alignment.center,
        margin: EdgeInsets.all(5),
        decoration: BoxDecoration(
          color: colorBackground,
          borderRadius: BorderRadius.circular(20),
        ),
        width: 100,
        height: 100,
        child: Column(
          children: <Widget>[
            Container(
              height: 35,
              // color: Colors.red,
              child: Text(title, style: styleBlue),
            ),
            Icon(
              icon,
              color: color,
              size: 36.0,
            ),
            Text(value, style: stylePink)
          ],
        ));
  }

  Widget formWidget() {
    return Container(
        margin: EdgeInsets.only(top: 20),
        // padding: EdgeInsets.symmetric(horizontal: 50),
        child: Column(
          children: [
            SwitchListTile(
              title: Text('Calambre en área pélvica'),
              value: _resp1,
              onChanged: (value) {
                setState(() {
                  _resp1 = value;
                });
              },
            ),
            SwitchListTile(
              title: Text('Fatiga'),
              value: _resp2,
              onChanged: (value) {
                setState(() {
                  _resp2 = value;
                });
              },
            ),
            SwitchListTile(
              title: Text('Hinchazón abdominal'),
              value: _resp3,
              onChanged: (value) {
                setState(() {
                  _resp3 = value;
                });
              },
            ),
            SwitchListTile(
              title: Text('Calambre en el vientre'),
              value: _resp4,
              onChanged: (value) {
                setState(() {
                  _resp4 = value;
                });
              },
            ),
            Container(
              padding: EdgeInsets.only(top: 5, left: 5, bottom: 5, right: 20),
              margin: EdgeInsets.only(bottom: 20),
              child: TextField(
                  controller: this._resp5,
                  keyboardType: TextInputType.multiline,
                  textInputAction: TextInputAction.newline,
                  minLines: 1,
                  maxLines: 5,
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.comment),
                    focusedBorder: InputBorder.none,
                    border: InputBorder.none,
                    hintText: 'Comentario ...',
                  )),
            )
          ],
        ));
  }

  Widget floatOpcion(UserProvider userService) {
    return FloatingActionButton(
      child: insert == false
          ? Icon(Icons.save)
          : CircularProgressIndicator(
              color: Colors.white,
            ),
      onPressed: () async {
        if (insert == false) {
          setState(() {
            insert = true;
          });
          var isSuccess = await userService.insertEmergency(
              _resp1, _resp2, _resp3, _resp4, _resp5.text);
          if (isSuccess == true) {
            toastGod('Se registro correctamente');
          } else {
            toastBad('Hubo un error');
          }
          setState(() {
            insert = false;
          });
        }
      },
    );
  }
}
