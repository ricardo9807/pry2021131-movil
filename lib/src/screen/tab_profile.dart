import 'package:flutter/material.dart';
import 'package:safeapp/src/preferences/preferencias_usuario.dart';
import 'package:safeapp/src/widget/boton_azul_widget.dart';
import 'package:jiffy/jiffy.dart';

class TabProfileScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final PreferenciasUsuario _preferencia = new PreferenciasUsuario();

    return Container(
      child: Scaffold(
        appBar: AppBar(
            centerTitle: true,
            title: Text('Perfil de la usuaria'),
            actions: <Widget>[
              Container(
                child: IconButton(
                  icon: Icon(Icons.calendar_today),
                  onPressed: () {
                    Navigator.pushNamed(context, 'report_hcpbs');
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.only(right: 10),
                child: IconButton(
                  icon: Icon(Icons.logout_sharp),
                  onPressed: () {
                    _preferencia.limpiarData();
                    Navigator.pushReplacementNamed(context, 'login');
                  },
                ),
              )
            ]),
        body: Container(
          child: Column(
            children: <Widget>[
              // Text('Perfil del usuario'),
              containerDatos(context, _preferencia),
              Center(
                child: BotonWithImg(
                    text: 'Actualizar',
                    onPressed: () {
                      onClickParametro(context);
                    },
                    color: Colors.pinkAccent,
                    icon: Icons.ac_unit),
              )
              // SizedBox(height: 15),
              // containerControles(context),
              // SizedBox(height: 15),
              // containerX(context),
              // SizedBox(height: 15),
              // containerCitas(context)
            ],
          ),
        ),
      ),
    );
  }

  Widget containerDatos(
      BuildContext context, PreferenciasUsuario _preferencia) {
    return Container(
      margin: EdgeInsets.all(10),
      padding: EdgeInsets.all(15),
      decoration: BoxDecoration(
        color: Colors.purple[50],
        borderRadius: BorderRadius.circular(20),
      ),
      child: Column(
        children: <Widget>[
          itemDatos('Nombre:', '${_preferencia.name}'),
          itemDatos('Apellido Paterno:', '${_preferencia.lastName}'),
          itemDatos('Apellido Materno:', '${_preferencia.surName}'),
          itemDatos('Dni:', '${_preferencia.documentNumber}'),
          itemDatos('Fecha de nacimineto:',
              '${Jiffy(_preferencia.birthdate).yMMMMd}'),
          itemDatos('Celular:', '${_preferencia.phone}'),
          itemDatos('Correo:', '${_preferencia.email}'),
          // BotonWithImg(
          //     text: 'Actualizar',
          //     onPressed: () {
          //       Navigator.pushReplacementNamed(context, 'home');
          //     },
          //     icon: Icons.ac_unit,
          //     color: Colors.pinkAccent)
        ],
      ),
    );
  }

  Widget itemDatos(String title, String value) {
    return Container(
      child: Row(
        children: <Widget>[
          Container(
            width: 120,
            child: Text(title),
          ),
          Container(
            width: 160,
            padding: EdgeInsets.only(left: 15),
            child: Text(
              value,
              style: TextStyle(color: Colors.purple),
            ),
          )
        ],
      ),
    );
  }

  Widget containerControles(BuildContext context) {
    return GestureDetector(
        child: Container(
          height: 70,
          margin: EdgeInsets.all(10),
          decoration: BoxDecoration(
            color: Colors.purple[50],
            borderRadius: BorderRadius.circular(20),
          ),
          child: Row(
            children: <Widget>[
              Icon(
                Icons.file_download_done,
                color: Colors.pink[300],
                size: 36.0,
              ),
              Text('Mis controles prenatales')
            ],
          ),
        ),
        onTap: () {
          Navigator.pushReplacementNamed(context, 'report_hcpbs');
        });
  }

  Widget containerX(BuildContext context) {
    return GestureDetector(
      child: Container(
        height: 70,
        margin: EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colors.purple[50],
          borderRadius: BorderRadius.circular(20),
        ),
        child: Row(
          children: <Widget>[
            Icon(
              Icons.attach_file,
              color: Colors.pink[300],
              size: 36.0,
            ),
            Text('X')
          ],
        ),
      ),
      onTap: () {
        Navigator.pushReplacementNamed(context, 'report_controls');
      },
    );
  }

  Widget containerCitas(BuildContext context) {
    return GestureDetector(
      child: Container(
        height: 70,
        margin: EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colors.purple[50],
          borderRadius: BorderRadius.circular(20),
        ),
        child: Row(
          children: <Widget>[
            Icon(
              Icons.archive,
              color: Colors.pink[300],
              size: 36.0,
            ),
            Text('Historias Clinicas Perinatales')
          ],
        ),
      ),
      onTap: () {
        Navigator.pushReplacementNamed(context, 'report_controls');
      },
    );
  }

  onClickParametro(BuildContext context) {
    print('onClickParametro');
    // Navigator.pushNamed(context, 'statistic_parameter');
    Navigator.pushNamed(context, 'statistic_parameter');
  }
}
