import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:safeapp/src/preferences/preferencias_usuario.dart';
import 'package:safeapp/src/providers/user_provider.dart';
import 'package:safeapp/src/util/toast_view.dart';

class StatisticParameterScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    UserProvider userProvider = Provider.of<UserProvider>(context);
    final phoneCtrl = TextEditingController();
    final addreCtrl = TextEditingController();
    final PreferenciasUsuario _preferencia = new PreferenciasUsuario();

    var f1 = _preferencia.phone;
    var f2 = _preferencia.address;
    phoneCtrl.text = f1;
    addreCtrl.text = f2;

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Actualizar Datos'),
        // leading: IconButton(
        //     icon: Icon(Icons.arrow_back),
        //     onPressed: () {
        //       Navigator.pushReplacementNamed(context, 'home');
        //     }),
      ),
      body: Container(
        width: double.infinity,
        margin: EdgeInsets.symmetric(horizontal: 15),
        child: Column(
          children: <Widget>[
            Container(
                // color: Colors.amber,
                child: Text(
              'Datos de contactos y de Ubigeo',
              style: TextStyle(fontSize: 20),
            )),
            itemParameter('Celular:', f1, phoneCtrl, 9, TextInputType.phone),

            itemParameter('Dirección:', f2, addreCtrl, 500, TextInputType.text),

            //itemParameter('Edad:', '30', 'años'),
            //itemParameter('Semana de gestación:', '6', 'Semanas'),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.save),
          onPressed: () async {
            var i = _preferencia.idUser;
            var p = phoneCtrl.text;
            var a = addreCtrl.text;
            print('mira::::::::::::::::::::::::::::::');
            print('i $i p $p a $a');
            if (p == '' || p == null || a == null || a == '') {
              await toastBad('Complete todos los campos del formulario');
              return;
            }
            bool isOk = await userProvider.updateUser(i, p, a);
            if (isOk) {
              await toastGod('Actualización correcta');
              Timer(Duration(seconds: 2), () async {
                Navigator.pushNamedAndRemoveUntil(
                    context, 'home', (route) => false);
              });
            } else {
              await toastBad('Complete todos los campos del formulario');
              Timer(Duration(seconds: 4), () async {
                Navigator.pushNamedAndRemoveUntil(
                    context, 'home', (route) => false);
              });
            }

            // Navigator.pushReplacementNamed(context, 'home');
          }),
    );
  }

  Widget itemParameter(
      String title,
      String value,
      TextEditingController textController,
      int long,
      TextInputType keyboardType) {
    return Container(
      height: 50,
      // color: Colors.red,
      child: Row(
        children: <Widget>[
          Container(
            // color: Colors.yellow,
            width: 85,
            child: Text(title),
          ),
          Container(
            width: 150,
            height: 40,
            padding: EdgeInsets.only(
              top: 0,
              left: 20,
            ),
            decoration: BoxDecoration(
                color: Colors.purple[200],
                borderRadius: BorderRadius.circular(20),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: Colors.black.withOpacity(0.05),
                      offset: Offset(0, 5),
                      blurRadius: 5)
                ]),
            child: TextField(
                controller: textController,
                maxLength: long,
                autocorrect: false,
                keyboardType: keyboardType,
                decoration: InputDecoration(
                    focusedBorder: InputBorder.none,
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.all(2.0),
                    // contentPadding: EdgeInsets.all(10.0),
                    counter: new SizedBox(
                      height: 0.0,
                    ))),
          ),
          Container(
            margin: EdgeInsets.only(left: 5),
            width: 90,
            //child: Text(medicion),
          ),
        ],
      ),
    );
  }
}
