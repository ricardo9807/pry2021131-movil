import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:safeapp/src/model/values_model.dart';
import 'package:safeapp/src/preferences/preferencias_usuario.dart';
import 'package:safeapp/src/providers/user_provider.dart';
import 'package:safeapp/src/widget/boton_azul_widget.dart';
import 'package:intl/intl.dart';
import 'package:jiffy/jiffy.dart';

class TabStatisticsScreen extends StatelessWidget {
  late UserProvider userService;
  late Size size;
  @override
  Widget build(BuildContext context) {
    final PreferenciasUsuario _preferencia = new PreferenciasUsuario();
    final userService = Provider.of<UserProvider>(context);
    size = MediaQuery.of(context).size;

    var now = new DateTime.now();
    var formatter = new DateFormat('yyyy-MM-dd');
    String formattedDate = formatter.format(now);
    print(formattedDate);
    return Container(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Datos'),
          elevation: 1,
          // backgroundColor: Colors.white,
          actions: <Widget>[
            Container(
              margin: EdgeInsets.only(right: 10),
              child: IconButton(
                icon: Icon(Icons.loop_outlined),
                onPressed: () async {
                  await userService.signalModelData();
                },
              ),
            ),
            Container(
              margin: EdgeInsets.only(right: 10),
              child: IconButton(
                icon: Icon(Icons.watch),
                onPressed: () {
                  Navigator.pushNamed(context, 'test');
                },
              ),
            ),
          ],
        ),
        body: SafeArea(
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: bodyTabStatistics(context, now, _preferencia),
          ),
        ),
      ),
    );
  }

  Widget bodyTabStatistics(BuildContext context, DateTime currentDate,
      PreferenciasUsuario _preferencia) {
    userService = Provider.of<UserProvider>(context);
    print('pulse ${userService.dataSignal.pulse}');
    print('bloodPressure ${userService.dataSignal.bloodPresure}');
    print('temperature ${userService.dataSignal.temperature}');
    print('diastolic ${userService.dataSignal.diastolic}');
    print('systolic ${userService.dataSignal.systolic}');

    return FutureBuilder(
        future: userService.getValues(),
        builder: (BuildContext context, AsyncSnapshot<ValuesModel> snapshot) {
          if (snapshot.hasData) {
            ValuesModel data = snapshot.data!;
            return Container(
              padding: EdgeInsets.symmetric(horizontal: 15),
              width: size.width,
              // color: Colors.red,
              child: Column(
                children: <Widget>[
                  Container(
                      width: size.width,
                      child: Text(
                        '${Jiffy(currentDate).yMMMMd}',
                        style: TextStyle(fontSize: 20),
                      )),
                  Container(
                    width: size.width,
                    child: Text('Hola ${_preferencia.name}'),
                  ),
                  Text('Mis signos vitales'),
                  Row(
                    children: <Widget>[
                      signoVitalWidget(
                        'Presión arterial',
                        '${userService.dataSignal.diastolic}/${userService.dataSignal.systolic} mmhg',
                        Colors.blue[50],
                        Icons.bloodtype,
                      ),
                      signoVitalWidget(
                          'Frecuencia Cardiaca',
                          '${userService.dataSignal.pulse} LPM',
                          Colors.amber[50],
                          Icons.favorite),
                      signoVitalWidget(
                        'Temperatura',
                        '${userService.dataSignal.temperature}',
                        Colors.orange[50],
                        Icons.thermostat,
                      )
                    ],
                  ),
                  Text('Mis parámetros Fisiológicos'),
                  containerParametros(context, data),
                  Text('Datos del embarazo'),
                  containerDatosEmbarazo(context, data),
                  Text('Mi médico a cargo'),
                  containerMedico(context, data),
                  BotonWithImg(
                      text: 'Notificar',
                      onPressed: () {
                        Navigator.pushNamed(context, 'emergency_notify');
                      },
                      icon: Icons.ac_unit,
                      color: Colors.pinkAccent)
                ],
              ),
            );
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }

  Widget containerParametros(BuildContext context, ValuesModel value) {
    return Container(
      margin: EdgeInsets.all(10),
      padding: EdgeInsets.all(15),
      decoration: BoxDecoration(
        color: Colors.purple[50],
        borderRadius: BorderRadius.circular(20),
      ),
      child: Column(
        children: <Widget>[
          itemDatos('Peso:', '${value.actualWeight} kg'),
          itemDatos('Altura:', '${value.pregnantHeight} cm'),
          itemDatos('Edad:', '${value.age} años'),
          itemDatos(
              'Edad gestacional (semanas):', '${value.currentGestationalAge}'),
          Center(
              // child: BotonWithImg(
              //     text: 'Actualizar',
              //     onPressed: () {
              //       onClickParametro(context);
              //     },
              //     color: Colors.pinkAccent,
              //     icon: Icons.ac_unit),
              )
        ],
      ),
    );
  }

  Widget containerDatosEmbarazo(BuildContext context, ValuesModel value) {
    return Container(
      margin: EdgeInsets.all(10),
      padding: EdgeInsets.all(15),
      decoration: BoxDecoration(
        color: Colors.orange[50],
        borderRadius: BorderRadius.circular(20),
      ),
      child: Column(
        children: <Widget>[
          itemDatos('Próximo control:', '${Jiffy(value.nextControl).yMMMMd}'),
          itemDatos('¿Es un embarazo múltiple?:', '${value.multiplePregnancy}'),
          itemDatos('Antecedentes de parto prematuro',
              '${value.previousPretermPregnancy}'),
          itemDatos('Clasificación:', '${value.classification}'),
          Center()
        ],
      ),
    );
  }

  Widget containerMedico(BuildContext context, ValuesModel value) {
    return Container(
      margin: EdgeInsets.all(10),
      padding: EdgeInsets.all(15),
      decoration: BoxDecoration(
        color: Colors.green[50],
        borderRadius: BorderRadius.circular(20),
      ),
      child: Column(
        children: <Widget>[
          itemDatos('Doctor:', '${value.specialist!.name}'),
          itemDatos('DNI:', '${value.specialist!.documentNumber}'),
          itemDatos(
              'N° Colegiatura:', '${value.specialist!.colegiatureNumber}'),
          itemDatos('Teléfono:', '${value.specialist!.phone}'),
          Center(
              // child: BotonWithImg(
              //     text: 'Actualizar',
              //     onPressed: () {
              //       onClickMedico(context);
              //     },
              //     color: Colors.pinkAccent,
              //     icon: Icons.ac_unit),
              )
        ],
      ),
    );
  }

  Widget itemDatos(String title, String value) {
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            width: 120,
            // color: Colors.red,
            child: Text(title),
          ),
          Container(
            width: 160,
            // color: Colors.blue,
            padding: EdgeInsets.only(left: 15),
            child: Text(
              value,
              style: TextStyle(color: Colors.purple),
            ),
          )
        ],
      ),
    );
  }

  Widget signoVitalWidget(
      String title, String value, Color? colorBackground, IconData icon) {
    Color? color = Colors.pink[200];
    TextStyle styleBlue = TextStyle(color: Colors.blue[200]);
    TextStyle stylePink = TextStyle(color: color);

    return Container(
        padding: EdgeInsets.all(5),
        alignment: Alignment.center,
        margin: EdgeInsets.all(5),
        decoration: BoxDecoration(
          color: colorBackground,
          borderRadius: BorderRadius.circular(20),
        ),
        width: 100,
        height: 100,
        child: Column(
          children: <Widget>[
            Container(
              height: 35,
              // color: Colors.red,
              child: Text(title, style: styleBlue),
            ),
            Icon(
              icon,
              color: color,
              size: 36.0,
            ),
            Text(value, style: stylePink)
          ],
        ));
  }

  onClickParametro(BuildContext context) {
    print('onClickParametro');
    Navigator.pushReplacementNamed(context, 'statistic_parameter');
  }

  onClickMedico(BuildContext context) {
    print('onClickMedico');
    Navigator.pushReplacementNamed(context, 'statistic_doctor');
  }
}
