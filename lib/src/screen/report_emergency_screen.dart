import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:safeapp/src/model/emergency_model.dart';
import 'package:safeapp/src/providers/user_provider.dart';
import 'package:safeapp/src/util/arrays_info.dart';
import 'package:jiffy/jiffy.dart';

class ReportEmergencyScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Jiffy.locale('es');
    final String idHcpb = ModalRoute.of(context)!.settings.arguments as String;
    final userService = Provider.of<UserProvider>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('Emergencias'),
      ),
      backgroundColor: Color(0xffF2F2F2),
      body: SafeArea(
        child: Container(
          child: FutureBuilder(
              future: userService.emergenciesByHcpb(idHcpb),
              builder: (BuildContext context,
                  AsyncSnapshot<List<EmergencyModel>> snapshot) {
                if (snapshot.hasData) {
                  List<EmergencyModel> emergencies = [];
                  emergencies = snapshot.data!;
                  return _crearListado(emergencies);
                }
                return Center(child: CircularProgressIndicator());
              }),
        ),
      ),
    );
  }

  Widget _crearListado(List<EmergencyModel> emergencies) {
    return ListView.builder(
        itemCount: emergencies.length,
        itemBuilder: (context, i) => _crearItem(context, emergencies[i]));
  }

  Widget _crearItem(BuildContext context, EmergencyModel emergency) {
    return Card(
        child: ListTile(
            leading: Icon(Icons.calendar_today),
            title: Text('${Jiffy(emergency.dateAttention).yMMMMd}'),
            subtitle: Text(
              '¿Se internó a la gestante? ${showYesOrNot(emergency.interPregnant)}',
            ),
            onTap: () async {
              Navigator.pushNamed(context, 'report_emergency_detail',
                  arguments: emergency);
            }));
  }
}
