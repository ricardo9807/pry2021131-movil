import 'dart:async';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:safeapp/src/providers/user_provider.dart';

class TabUbicacionScreen extends StatefulWidget {
  static final CameraPosition _kLake = CameraPosition(
      bearing: 192.8334901395799,
      target: LatLng(-11.962411, -77.0706646),
      tilt: 59.440717697143555,
      zoom: 19.151926040649414);

  @override
  _TabUbicacionScreenState createState() => _TabUbicacionScreenState();
}

class _TabUbicacionScreenState extends State<TabUbicacionScreen> {
  Completer<GoogleMapController> _controller = Completer();
  bool gpsIsActived = false;
  Map<MarkerId, Marker> _markers = <MarkerId, Marker>{
    MarkerId('1'): Marker(
        markerId: MarkerId('1'),
        position: LatLng(-11.962411, -77.0706646),
        infoWindow: InfoWindow(title: "Clínica #1", snippet: "Sede Principal")),
    MarkerId('2'): Marker(
        markerId: MarkerId('2'),
        position: LatLng(-11.968341, -77.083277),
        infoWindow:
            InfoWindow(title: "Clínica #2", snippet: "Sede Secundaria")),
  };

  @override
  Widget build(BuildContext context) {
    UserProvider user = Provider.of<UserProvider>(context);
    final initialCameraPosition = new CameraPosition(
        target: LatLng(user.ubicacion.latitude, user.ubicacion.longitude),
        zoom: 15);

    print('mi ub es ${user.ubicacion.latitude} - ${user.ubicacion.longitude}');
    return Container(
      child: Scaffold(
        appBar: AppBar(title: Text('Centros de salud cercanos')),
        body: FutureBuilder(
            future: validarPermiso(),
            builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
              if (snapshot.hasData) {
                var data = snapshot.data;
                print('dash data $data || gpsIsActived $gpsIsActived');
                if (data == true && gpsIsActived == true) {
                  return Stack(
                    children: [
                      GoogleMap(
                          initialCameraPosition: initialCameraPosition,
                          myLocationEnabled: true,
                          myLocationButtonEnabled: false,
                          zoomControlsEnabled: false,
                          mapType: MapType.normal,
                          markers: Set<Marker>.of(_markers.values),
                          onCameraMove: (camaraPos) {
                            print('--> ${camaraPos.target}');
                          },
                          onMapCreated: (GoogleMapController controller) {
                            _controller.complete(controller);
                          }),
                      // Center(
                      //   child: Transform.translate(
                      //     offset: Offset(0, -10),
                      //     child: Icon(Icons.location_on),
                      //   ),
                      // )
                    ],
                  );
                } else {
                  return Container(
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text('Es necesario el GPS para usar esta app'),
                          MaterialButton(
                            color: Colors.blue,
                            child: Text(
                              'Cargar',
                              style: TextStyle(color: Colors.white),
                            ),
                            onPressed: () async {
                              await validGps();
                            },
                          )
                        ],
                      ),
                    ),
                  );
                }
              } else {
                return Container();
              }
            }),
      ),
    );
  }

  Future<void> _goToTheLake() async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(
        CameraUpdate.newCameraPosition(TabUbicacionScreen._kLake));
  }

  Future<bool> validarPermiso() async {
    var serviceEnabled = await Geolocator.isLocationServiceEnabled();
    print('dash validarPermiso $serviceEnabled');
    if (serviceEnabled) {
      setState(() {
        gpsIsActived = true;
      });
    }

    return (serviceEnabled) ? true : false;
  }

  validGps() async {
    var serviceEnabled = await Geolocator.isLocationServiceEnabled();
    print('dash validGps $serviceEnabled');
    if (serviceEnabled) {
      setState(() {
        gpsIsActived = true;
      });
    }
  }
}
