import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:safeapp/src/providers/user_provider.dart';
import 'package:safeapp/src/util/toast_view.dart';
import 'package:safeapp/src/widget/boton_azul_widget.dart';
import 'package:safeapp/src/widget/custom_input_widget.dart';
import 'package:safeapp/src/widget/label_widget.dart';
import 'package:safeapp/src/widget/logo_widget.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffF2F2F2),
      body: SafeArea(
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Container(
            height: MediaQuery.of(context).size.height * 0.9,
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  LogoWidget(titulo: ''),
                  _Form(),
                  LabelWidget(
                    ruta: '',
                    titulo: '',
                    subTitulo: '',
                  ),
                  GestureDetector(
                    child: Text('',
                        style: TextStyle(
                          color: Colors.blue[600],
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        )),
                    onTap: () {
                      Navigator.pushReplacementNamed(
                          context, '');
                    },
                  ),
                  Text(
                    '',
                    style: TextStyle(fontWeight: FontWeight.w200),
                  )
                ]),
          ),
        ),
      ),
    );
  }
}

class _Form extends StatefulWidget {
  @override
  __FormState createState() => __FormState();
}

class __FormState extends State<_Form> {
  final emailCtrl = TextEditingController();
  final passCtrl = TextEditingController();

  @override
  Widget build(BuildContext context) {
    UserProvider login = Provider.of<UserProvider>(context);

    return Container(
      margin: EdgeInsets.only(top: 20),
      padding: EdgeInsets.symmetric(horizontal: 50),
      child: Column(children: <Widget>[
        CustomInputWidget(
          icon: Icons.mail_outline,
          placeholder: 'Correo',
          textController: emailCtrl,
          keyboardType: TextInputType.emailAddress,
        ),
        CustomInputWidget(
          icon: Icons.lock_outline,
          placeholder: 'Contraseña',
          textController: passCtrl,
          isPassword: true,
        ),
        BotonAzulWidget(
          text: 'Ingresar',
          onPressed: () => {onClick(login)},
        )
      ]),
    );
  }

  onClick(UserProvider login) async {
    // print(emailCtrl.text); user_paciente2@gmail.com
    // print(passCtrl.text); 99551101
    // bool isSuccess = await login.login('user_paciente2@gmail.com', '2021nuevo');
    String isSuccess = await login.login(emailCtrl.text, passCtrl.text);
    // print('isSuccess');
    // print(isSuccess);
    if (isSuccess != 'Ok') {
      toastBad(isSuccess);
      return;
    }

    Navigator.pushReplacementNamed(context, 'home');
  }
}
