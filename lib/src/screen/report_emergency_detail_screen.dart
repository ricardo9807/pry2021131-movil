import 'package:flutter/material.dart';
import 'package:safeapp/src/model/emergency_model.dart';
import 'package:jiffy/jiffy.dart';
import 'package:safeapp/src/util/arrays_info.dart';
import 'package:safeapp/src/model/fetuses_detail_model.dart';

class ReportEmergencyDetailScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Jiffy.locale('es');
    final EmergencyModel emergencyArgument = ModalRoute.of(context)!.settings.arguments
        as EmergencyModel; 

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Reporte de emergencia'),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
              padding: EdgeInsets.all(20.0),
              child: Column(children: [
                datoGeneral(emergencyArgument),
                SizedBox(height: 15),
                datoGestante(emergencyArgument),
                SizedBox(height: 15),
                datoFetos(emergencyArgument.fetusesDetail!),
                SizedBox(height: 15),
                datoDiagnostico(emergencyArgument),
              ])),
        ),
      ),
    );
  }

  Widget datoGeneral(EmergencyModel emergencyArgument) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5.0),
        boxShadow: <BoxShadow>[
          BoxShadow(
              color: Colors.black26,
              blurRadius: 3.0,
              offset: Offset(0.0, 5.0),
              spreadRadius: 3.0),
        ],
      ),
      child: Column(children: [
        AppBar(
          title: const Text('Datos generales', style: TextStyle(
            color: Colors.black
          )), 
          automaticallyImplyLeading: false,
          backgroundColor: Colors.purple[50],
        ),
        ListTile(
          title: Text(
            '${Jiffy(emergencyArgument.dateAttention).yMMMMd}',
            style: TextStyle(fontSize: 20),
          ),
          subtitle: Text('Fecha de la Emergencia'),
        ),
        ListTile(
          title: Text(
             '${emergencyArgument.initialObservation}',
            style: TextStyle(fontSize: 20),
          ),
          subtitle: Text('Observación al ingresar'),
        ),
        ListTile(
          title: Text(
             '${emergencyArgument.clinic}',
            style: TextStyle(fontSize: 20),
          ),
          subtitle: Text('Centro de Salud'),
        ),
        ListTile(
          title: Text(
            '${Jiffy(emergencyArgument.lastControlDate).yMMMMd}',
            style: TextStyle(fontSize: 20),
          ),
          subtitle: Text('Fecha del Último Control'),
        ),
      ]),
    );
  }

  Widget datoGestante(EmergencyModel emergencyArgument){
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5.0),
        boxShadow: <BoxShadow>[
          BoxShadow(
              color: Colors.black26,
              blurRadius: 3.0,
              offset: Offset(0.0, 5.0),
              spreadRadius: 3.0),
        ],
      ),
      child: Column(children: [
        AppBar(
          title: const Text('Evaluación de la gestante', style: TextStyle(
            color: Colors.black
          )),          
          automaticallyImplyLeading: false,
          backgroundColor: Colors.purple[50],
        ),
        ListTile(
          title: Text(
            '${emergencyArgument.gestationalAge} semanas',
            style: TextStyle(fontSize: 20),
          ),
          subtitle: Text('Edad Gestacional'),
        ),
        ListTile(
          title: Text(
            '${emergencyArgument.diastolicPressure}',
            style: TextStyle(fontSize: 20),
          ),
          subtitle: Text('Presión diastólica'),
        ),
        ListTile(
          title: Text(
            '${emergencyArgument.systolicPressure}',
            style: TextStyle(fontSize: 20),
          ),
          subtitle: Text('Presión sistólica'),
        ),
        ListTile(
          title: Text(
            '${emergencyArgument.temperature} °',
            style: TextStyle(fontSize: 20),
          ),
          subtitle: Text('Temperatura'),
        ),
        ListTile(
          title: Text(
            '${emergencyArgument.pulse}',
            style: TextStyle(fontSize: 20),
          ),
          subtitle: Text('Pulso'),
        ),
        ListTile(
          title: Text(
            '${emergencyArgument.breathingFrequency}',
            style: TextStyle(fontSize: 20),
          ),
          subtitle: Text('Frecuencia respiratoria'),
        ),
      ]),
    );
  }

  Widget datoFeto(EmergencyModel emergencyArgument) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5.0),
        boxShadow: <BoxShadow>[
          BoxShadow(
              color: Colors.black26,
              blurRadius: 3.0,
              offset: Offset(0.0, 5.0),
              spreadRadius: 3.0),
        ],
      ),
      child: Column(children: [
        AppBar(
          title: const Text('Evolución del feto', style: TextStyle(
            color: Colors.black
          )),          
          automaticallyImplyLeading: false,
          backgroundColor: Colors.purple[50],
        ),        
        ListTile(
          title: Text(
            'Nutriologo 1',
            style: TextStyle(fontSize: 20),
          ),
          subtitle: Text('FCF'),
        ),
        ListTile(
          title: Text(
            'Perez',
            style: TextStyle(fontSize: 20),
          ),
          subtitle: Text('Situación'),
        ),
        ListTile(
          title: Text(
            '10101014',
            style: TextStyle(fontSize: 20),
          ),
          subtitle: Text('Presentación del Feto'),
        ),
        ListTile(
          title: Text(
            '10101014',
            style: TextStyle(fontSize: 20),
          ),
          subtitle: Text('Posición'),
        ),
        ListTile(
          title: Text(
            '10101014',
            style: TextStyle(fontSize: 20),
          ),
          subtitle: Text('Observación'),
        ),
      ]),
    );
  }

  Widget datoFetos(List<FetusesDetailModel> data) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5.0),
        boxShadow: <BoxShadow>[
          BoxShadow(
              color: Colors.black26,
              blurRadius: 3.0,
              offset: Offset(0.0, 5.0),
              spreadRadius: 3.0),
        ],
      ),
      child:Column(children: [
          AppBar(
            title: const Text('Estado de los fetos',
                style: TextStyle(color: Colors.black)),
            automaticallyImplyLeading: false,
            backgroundColor: Colors.purple[50],
          ),
          _crearListado(data),
        ])); 
      //_crearListado(data),
 
  }

  Widget _crearListado(List<FetusesDetailModel> data) {
    // return Text('data');
    return ListView.builder(
        shrinkWrap: true,
        itemCount: data.length,
        itemBuilder: (context, i) => _crearItem(context, data[i]));
  }

  Widget _crearItem(BuildContext context, FetusesDetailModel fetus) {
    return Card(
      
        child: ListTile(
            leading: Icon(Icons.receipt),
            title: Text('Estado del Feto #${fetus.positionFetus}'),
            subtitle: Text('Ver detalle del feto'),
            onTap: () async {
              // Navigator.pushNamed(context, 'report_hcpbs_detail',
              //     arguments: hcpb);
              _mostrarAlerta(context, fetus);
            }));
  }

  void _mostrarAlerta(BuildContext context, FetusesDetailModel fetus) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return AlertDialog(
            title: Text('Datos del feto'),
            content: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  ListTile(
                    title: Text(
                      '${fetus.observation}',
                      style: TextStyle(fontSize: 18),
                    ),
                    subtitle: Text('Observación'),
                  ),
                  ListTile(
                    title: Text(
                      '${fetus.fcf}',
                      style: TextStyle(fontSize: 18),
                    ),
                    subtitle: Text('Frecuencia cardiaca fetal'),
                  ),
                  ListTile(
                    title: Text(
                      '${fetusDetail('positionFetus',fetus.positionFetus)}',
                      style: TextStyle(fontSize: 18),
                    ),
                    subtitle: Text('Posición del feto'),
                  ),
                  ListTile(
                    title: Text(
                      '${fetusDetail('situationFetus',fetus.situationFetus)}',
                      style: TextStyle(fontSize: 18),
                    ),
                    subtitle: Text('Situación del feto'),
                  ),
                  ListTile(
                    title: Text(
                      '${fetusDetail('presentationFetus',fetus.presentationFetus)}',
                      style: TextStyle(fontSize: 18),
                    ),
                    subtitle: Text('Presentación del feto'),
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              TextButton(
                  child: Text('Ok'),
                  onPressed: () => Navigator.of(context).pop())
            ],
          );
        });
  }

  Widget datoDiagnostico(EmergencyModel emergencyArgument) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5.0),
        boxShadow: <BoxShadow>[
          BoxShadow(
              color: Colors.black26,
              blurRadius: 3.0,
              offset: Offset(0.0, 5.0),
              spreadRadius: 3.0),
        ],
      ),
      child: Column(children: [
        AppBar(
          title: const Text('Diagnóstico y recomendaciones', style: TextStyle(
            color: Colors.black,
            fontSize: 16,
          )),
          automaticallyImplyLeading: false,
          backgroundColor: Colors.purple[50],
        ),
        ListTile(
          title: Text(
            '${emergencyArgument.systemDiagnosis}',
            style: TextStyle(fontSize: 15),
          ),
          subtitle: Text('Diagnostico del sistema'),
        ),
        ListTile(
          title: Text(
            '${emergencyArgument.specialistName}',
            style: TextStyle(fontSize: 15),
          ),
          subtitle: Text('Especialista encargado'),
        ),
        ListTile(
          title: Text(
            '${emergencyArgument.colegiatureNumber}',
            style: TextStyle(fontSize: 15),
          ),
          subtitle: Text('Número de Colegiatura'),
        ),
        ListTile(
          title: Text(
            '${emergencyArgument.finalDiagnosis}',
            style: TextStyle(fontSize: 15),
          ),
          subtitle: Text('Diagnostico del especialista'),
        ),
        ListTile(
          title: Text(
            '${emergencyArgument.recommendations}',
            style: TextStyle(fontSize: 15),
          ),
          subtitle: Text('Recomendaciones'),
        ),
        ListTile(
          title: Text(
            '${showYesOrNot(emergencyArgument.interPregnant)}',
            style: TextStyle(fontSize: 15),
          ),
          subtitle: Text('¿Se internó a la gestante?'),
        ),
        ListTile(
          title: Text(
            '${showYesOrNot(emergencyArgument.terminatePregnancy)}',
            style: TextStyle(fontSize: 15),
          ),
          subtitle: Text('¿Se interrumpió?'),
        )
      ]),
    );
  }
}

