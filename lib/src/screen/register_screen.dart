import 'package:flutter/material.dart';
import 'package:safeapp/src/widget/boton_azul_widget.dart';
import 'package:safeapp/src/widget/custom_input_widget.dart';
import 'package:safeapp/src/widget/label_widget.dart';

class RegisterScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffF2F2F2),
      body: SafeArea(
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Container(
            height: MediaQuery.of(context).size.height * 0.9,
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  // LogoWidget(titulo: 'Registro'),
                  _Form(),
                  LabelWidget(
                    ruta: 'login',
                    titulo: 'Ya tienes cuenta?',
                    subTitulo: 'Ingresa ahora!',
                  ),
                  Text(
                    'Términos y condiciones de uso',
                    style: TextStyle(fontWeight: FontWeight.w200),
                  )
                ]),
          ),
        ),
      ),
    );
  }
}

class _Form extends StatefulWidget {
  @override
  __FormState createState() => __FormState();
}

class __FormState extends State<_Form> {
  final nameCtrl = TextEditingController();
  final apePaCtrl = TextEditingController();
  final apeMaCtrl = TextEditingController();
  final dniCtrl = TextEditingController();
  final dateCtrl = TextEditingController();
  final phoneCtrl = TextEditingController();
  final emailCtrl = TextEditingController();
  final pass1Ctrl = TextEditingController();
  final pass2Ctrl = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 20),
      padding: EdgeInsets.symmetric(horizontal: 15),
      child: Column(children: <Widget>[
        CustomInputWidget(
          icon: Icons.person,
          placeholder: 'Nombre',
          textController: nameCtrl,
          keyboardType: TextInputType.name,
        ),
        CustomInputWidget(
          icon: Icons.person,
          placeholder: 'Apellido Paterno',
          textController: apePaCtrl,
          keyboardType: TextInputType.name,
        ),
        CustomInputWidget(
          icon: Icons.person,
          placeholder: 'Apellido Materno',
          textController: apeMaCtrl,
          keyboardType: TextInputType.name,
        ),
        CustomInputWidget(
          icon: Icons.phone,
          placeholder: 'Télefono Celular',
          textController: phoneCtrl,
          keyboardType: TextInputType.phone,
        ),
        CustomInputWidget(
          icon: Icons.mail_outline,
          placeholder: 'Correo',
          textController: emailCtrl,
          keyboardType: TextInputType.emailAddress,
        ),
        CustomInputWidget(
          icon: Icons.lock_outline,
          placeholder: 'Contraseña',
          textController: pass1Ctrl,
          isPassword: true,
        ),
        CustomInputWidget(
          icon: Icons.lock_outline,
          placeholder: 'Confirmar Contraseña',
          textController: pass1Ctrl,
          isPassword: true,
        ),
        BotonAzulWidget(
          text: 'Registrar',
          onPressed: onClick,
        )
      ]),
    );
  }

  onClick() {
    print('pipipisssssssssssssssssssss');
    // print(emailCtrl.text);
    // print(passCtrl.text);
  }
}
