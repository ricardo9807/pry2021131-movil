import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:safeapp/src/model/control_model.dart';
import 'package:safeapp/src/model/hcpb_model.dart';
import 'package:safeapp/src/model/signal_model.dart';
import 'package:safeapp/src/model/user_model.dart';
import 'package:safeapp/src/model/emergency_model.dart';
import 'package:safeapp/src/model/values_model.dart';
import 'package:safeapp/src/preferences/preferencias_usuario.dart';
import 'package:safeapp/src/util/var-globals.dart' as global;
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart' show LatLng;
import 'package:http/http.dart' as http;
import 'package:rxdart/rxdart.dart';

class UserProvider extends ChangeNotifier {
  late StreamSubscription<Position> _positionSubs;
  LatLng ubicacion = new LatLng(0.0, 0.0);
  final PreferenciasUsuario _preferencia = new PreferenciasUsuario();
  final Map<String, String> headers = {"Accept": "application/json"};
  final _valoresSignal = BehaviorSubject<SignalModel>();
  SignalModel dataSignal = SignalModel(
      pulse: 0, bloodPresure: 0, temperature: 00.0, diastolic: 0, systolic: 0);
  final hcpbs = [];
  UserProvider();

  Stream<SignalModel> get valoresSignalStream => _valoresSignal.stream;
  Future<String> login(String email, String password) async {
    String loginResult= '';
    try {
      final url = Uri.parse('${global.BASE_URL}/users/signIn');
      var body = {'email': email, 'password': password};
      final respuesta = await http.post(url, headers: headers, body: body);
      if(respuesta.statusCode == 200 ){
        var respuestaDecode = json.decode(respuesta.body);
        _preferencia.idUser = respuestaDecode['_id'];
        _preferencia.name = respuestaDecode['name'];
        _preferencia.lastName = respuestaDecode['lastName'];
        _preferencia.surName = respuestaDecode['surName'];
        _preferencia.documentNumber = respuestaDecode['documentNumber'];
        _preferencia.birthdate = respuestaDecode['birthdate'];
        _preferencia.phone = respuestaDecode['phone'];
        _preferencia.email = respuestaDecode['email'];
        loginResult = 'Ok';
      }
      if(respuesta.statusCode == 400 ){
        print('error::::::::::::::');
        print(respuesta);
        var respuestaDecode = json.decode(respuesta.body);
        loginResult = respuestaDecode['message'];
      }
    } catch (error) {
      print(error);
    }

    return loginResult;
  }

  Future<UserModel> create(UserModel model) async {
    UserModel user = new UserModel();
    print('creando ...');

    try {
      final url = Uri.parse('${global.BASE_URL}/${global.URL_PACIENTE}');
      final respuesta = await http.post(url, body: model.toJson(), headers: {
        "Content-type": "application/json",
        "Accept": "application/json",
      });
      print(respuesta.body);
    } catch (error) {
      print(error);
    }

    return user;
  }

  Future<UserModel> findUser(id) async {
    UserModel user = new UserModel();
    try {
      final url =
          Uri.parse('http://localhost:3000/patients/6131cfef1e33c00f1ccbf386');
      final respuesta = await http.get(url, headers: {
        "Content-type": "application/json",
        "Accept": "application/json",
      });
      user = UserModel.fromJson(respuesta.body);
    } catch (error) {
      print(error);
    }

    return user;
  }

  Future<List<HcpbModel>> hcpbsByUser() async {
    final List<HcpbModel> hcpbs = [];
    var id = _preferencia.idUser;
    try {
      var url = Uri.parse('${global.BASE_URL}/hcpbs/patient/$id');
      var response = await http.get(url);

      return hcpbModelFromJson(response.body);
    } catch (e) {
      print('Error hcpbsByUser: $e');
    }
    return hcpbs;
  }

  Future<List<EmergencyModel>> emergenciesByHcpb(String id) async {
    final List<EmergencyModel> emergencies = [];
    try {
      print('print emergenciesByHcpb try');
      var url =
          Uri.parse('${global.BASE_URL}/emergencies/$id/getAllEmergencies');
      print('url::::::::::::');
      print(url);
      var response = await http.get(url);
      List<EmergencyModel> data = emergencyModelFromJson(response.body);
      return data;
    } catch (e) {
      print('print emergenciesByHcpb catch');
      print('Error: $e');
    }
    return emergencies;
  }

  Future<List<ControlModel>> controlsByHcpb(String id) async {
    final List<ControlModel> controls = [];
    try {
      var url =
          Uri.parse('${global.BASE_URL}/prenatalControls/$id/getAllControls');
      print('----> ${global.BASE_URL}/prenatalControls/$id/getAllControls');
      var response = await http.get(url);
      List<ControlModel> data = controlModelFromJson(response.body);
      return data;
    } catch (e) {
      print('print controlsByHcpb catch');
      print('Error: $e');
    }
    return controls;
  }

  signalModelData() async {
    // notifyListeners();
    print('tiempo -----> signalModelData');
    try {
      var url = Uri.parse('${global.BASE_URL}/emergencies/showVal');
      var response = await http.get(url);
      SignalModel data1 = signalModelFromJson(response.body);
      dataSignal.pulse = data1.pulse;
      dataSignal.bloodPresure = data1.bloodPresure;
      dataSignal.temperature = data1.temperature;
      dataSignal.diastolic = data1.diastolic;
      dataSignal.systolic = data1.systolic;
    } catch (e) {
      print('Error signalModelData : $e');
    }
    notifyListeners();
  }

  Future<bool> insertEmergency(
      bool r1, bool r2, bool r3, bool r4, String r5) async {
    bool res = false;
    try {
      var url = Uri.parse('${global.BASE_URL}/emergencies/notifyEmergency');
      var body = {
        'idPregnant': _preferencia.idUser,
        'temperature': '${dataSignal.temperature}',
        'pulse': '${dataSignal.pulse}',
        'bloodPressure': '${dataSignal.bloodPresure}',
        'diastolic': '${dataSignal.diastolic}',
        'systolic': '${dataSignal.systolic}',
        'crampPelvicArea': '$r1',
        'fatigue': '$r2',
        'abdominalPain': '$r3',
        'swelling': '$r4',
        'patientComment': r5
      };
      await http.post(url, headers: headers, body: body);
      res = true;
    } catch (e) {
      print('insertEmergency ERROR $e');
    }
    return res;
  }

  Future<ValuesModel> getValues() async {
    ValuesModel data = new ValuesModel();
    var id = _preferencia.idUser;
    try {
      var url = Uri.parse('${global.BASE_URL}/patients/$id/getValues');
      var response = await http.get(url);
      return valuesModelFromJson(response.body);
    } catch (e) {
      print('Error insertEmergency: $e');
    }
    return data;
  }

  Future<SignalModel> signalModelData2() async {
    SignalModel data = new SignalModel(
        pulse: 0,
        bloodPresure: 0,
        temperature: 37.0,
        diastolic: 0,
        systolic: 0);
    var id = _preferencia.idUser;
    try {
      var url = Uri.parse('${global.BASE_URL}/emergencies/showVal');
      var response = await http.get(url);
      return signalModelFromJson(response.body);
    } catch (e) {
      print('Error insertEmergency: $e');
    }
    return data;
  }

  void iniciarSeguimiento() {
    this._positionSubs = Geolocator.getPositionStream(
            desiredAccuracy: LocationAccuracy.high, distanceFilter: 10)
        .listen((Position position) {
      print('---------');
      ubicacion = LatLng(position.latitude, position.longitude);
      print(position);
      print('---------');
    });
  }

  Future<bool> updateUser(id, phone, address) async {
    bool respuesta = false;
    try {
      var url = Uri.parse('${global.BASE_URL}/patients/$id');
      var body = {'id': '$id', 'phone': '$phone', 'address': '$address'};

      if(phone != '' && phone != null && address != null && address != ''){
        final respuestaAPI = await http.put(url, headers: headers, body: body);
        var respuestaDecode = json.decode(respuestaAPI.body);
        print('----> ${respuestaDecode['user']}');
        _preferencia.phone = respuestaDecode['user']['phone'];
        _preferencia.address = respuestaDecode['user']['address'];
        respuesta = true;
      }else{
        respuesta = false;
      }

    } catch (e) {
      print('Error updateUser: $e');
    }
    return respuesta;
  }

  void cancelarSeguimiento() {
    this._positionSubs.cancel();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _valoresSignal.close();
  }
}
