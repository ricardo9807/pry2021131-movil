import 'package:flutter/material.dart';
import 'package:safeapp/src/screen/emergency_notify_screen.dart';
import 'package:safeapp/src/screen/home_screen.dart';
import 'package:safeapp/src/screen/loading_screen.dart';
import 'package:safeapp/src/screen/login_screen.dart';
import 'package:safeapp/src/screen/password_forget_screen.dart';
import 'package:safeapp/src/screen/register_screen.dart';
import 'package:safeapp/src/screen/reloj_screen.dart';
import 'package:safeapp/src/screen/report_controls_detail_screen.dart';
import 'package:safeapp/src/screen/report_controls_screen.dart';
import 'package:safeapp/src/screen/report_emergency_detail_screen.dart';
import 'package:safeapp/src/screen/report_emergency_screen.dart';
import 'package:safeapp/src/screen/report_hcpbs_detail_screen.dart';
import 'package:safeapp/src/screen/report_hcpbs_screen.dart';
import 'package:safeapp/src/screen/statistic_doctor_screen.dart';
import 'package:safeapp/src/screen/statistic_parameter_screen.dart';
import 'package:safeapp/src/screen/vincular_3_screen.dart';
import 'package:safeapp/src/screen/vincular_screen.dart';
import 'package:safeapp/src/screen/wearable_connect_screen.dart';
import 'package:safeapp/src/screen/welcome_screen.dart';

final Map<String, Widget Function(BuildContext)> appRoutes = {
  'home': (_) => HomeScreen(),
  'loading': (_) => LoadingScreen(),
  'login': (_) => LoginScreen(),
  'register': (_) => RegisterScreen(),
  'password_forget': (_) => PasswordForgetScreen(),
  'welcome': (_) => WelcomeScreen(),
  'wearable': (_) => WearableConnectScreen(),
  'statistic_doctor': (_) => StatisticDoctorScreen(),
  'statistic_parameter': (_) => StatisticParameterScreen(),
  'report_hcpbs': (_) => ReportHcpbsScreen(),
  'report_emergency': (_) => ReportEmergencyScreen(),
  'report_controls': (_) => ReportControlsScreen(),
  'report_hcpbs_detail': (_) => ReportHcpbsDetailScreen(),
  'report_emergency_detail': (_) => ReportEmergencyDetailScreen(),
  'report_controls_detail': (_) => ReportControlsDetailScreen(),
  'emergency_notify': (_) => EmergencyNotifyScreen(),
  'reloj': (_) => RelojScreen(),
  'blue': (_) => FlutterBlueApp(),
  'test': (_) => Test3Screen(),
};
