import 'package:safeapp/src/util/var-globals.dart' as global;
import 'package:http/http.dart' as http;

Future <String> showSpecialist() async {
  String fullName = 'Hola';
  var url = Uri.parse('${global.BASE_URL}/specialists/');
  var response =  await http.get(url);
  print('response:::::::');
  print(response);
  return fullName;
}
String fetusDetail(String type, int id) {
  String fullName = '-';
  if(type =='presentationFetus') {
    if (id == 1) return 'Cefálico';
    if (id == 2) return 'Podálico';
  }
  if(type == 'situationFetus'){
    if (id == 1) return 'Longitudinal';
    if (id == 2) return 'Transversal';
  }
  if(type == 'positionFetus'){
    if (id == 1) return 'Derecho';
    if (id == 2) return 'Izquierdo';
  }
  return fullName;
}
String showAnticopcetivo(String id) {
  String respuesta = '';
  switch (id) {
    case "0":
      respuesta = 'Ninguno';
      break;
    case "1":
      respuesta = 'Preservativo';
      break;
    case "2":
      respuesta = 'Óvulo';
      break;
    case "3":
      respuesta = 'Inyectable';
      break;
    case "4":
      respuesta = 'Pastilla';
      break;
    case "5":
      respuesta = 'Implante';
      break;
    case "6":
      respuesta = 'Natural';
      break;
    default:
      respuesta = 'Ninguno';
      break;
  }
  return respuesta;
}

String showYesOrNot(int id) {
  if (id == 0) return 'No';
  if (id == 1) return 'Si';
  return '--';
}

String showHCPBStatus(String status) {
  String respuesta = '';
  switch (status) {
    case "MONITORING":
      respuesta = 'En monitoreo';
      break;
    case "TERMINATED":
      respuesta = 'Terminado';
      break;
    case "TERMINATED_PRETERM_BIRTH":
      respuesta = 'Terminado con parto pretérmino';
      break;
  }
  return respuesta;
}