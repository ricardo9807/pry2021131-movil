import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

toastGod(String mensaje) async {
  await Fluttertoast.showToast(msg: mensaje);
}

toastBad(String mensaje) async {
  Fluttertoast.showToast(
      msg: mensaje,
      fontSize: 18,
      gravity: ToastGravity.BOTTOM,
      backgroundColor: Colors.red,
      textColor: Colors.white);
}
