import 'package:shared_preferences/shared_preferences.dart';
import 'package:rxdart/rxdart.dart';
import 'package:rxdart/subjects.dart';

class PreferenciasUsuario {
  static final PreferenciasUsuario _instancia =
      new PreferenciasUsuario._internal();

  factory PreferenciasUsuario() {
    return _instancia;
  }
  PreferenciasUsuario._internal();
  final _phoneController = BehaviorSubject<String>();
  final _addreController = BehaviorSubject<String>();
  Stream<String> get phoneStream => _phoneController.stream;
  Stream<String> get addreStream => _addreController.stream;

  // Obtener el último valor ingresado a los streams
  String? get phoneS => _phoneController.value;
  String? get addreS => _addreController.value;
  late SharedPreferences _prefs;

  initPreferencias() async {
    this._prefs = await SharedPreferences.getInstance();
  }

  String get idUser {
    return _prefs.getString('iduser') ?? '';
  }

  set idUser(String value) {
    _prefs.setString('iduser', value);
  }

  String get name {
    return _prefs.getString('name') ?? '';
  }

  set name(String value) {
    _prefs.setString('name', value);
  }

  String get lastName {
    return _prefs.getString('lastName') ?? '';
  }

  set lastName(String value) {
    _prefs.setString('lastName', value);
  }

  String get surName {
    return _prefs.getString('surName') ?? '';
  }

  set surName(String value) {
    _prefs.setString('surName', value);
  }

  String get documentNumber {
    return _prefs.getString('documentNumber') ?? '';
  }

  set documentNumber(String value) {
    _prefs.setString('documentNumber', value);
  }

  String get birthdate {
    return _prefs.getString('birthdate') ?? '';
  }

  set birthdate(String value) {
    _prefs.setString('birthdate', value);
  }

  String get phone {
    return _prefs.getString('phone') ?? '';
  }

  set phone(String value) {
    _prefs.setString('phone', value);
  }

  String get email {
    return _prefs.getString('email') ?? '';
  }

  set email(String value) {
    _prefs.setString('email', value);
  }

  String get address {
    return _prefs.getString('address') ?? '';
  }

  set address(String value) {
    _prefs.setString('address', value);
  }

  void limpiarData() {
    idUser = '';
    name = '';
    lastName = '';
    surName = '';
    documentNumber = '';
    birthdate = '';
    phone = '';
    email = '';
    address = '';
  }

  dispose() {
    _phoneController.close();
    _addreController.close();
  }
}
