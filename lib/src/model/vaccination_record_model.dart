import 'dart:convert';

List<VaccinationRecordModel> vaccinationRecordModelFromJson(String str) =>
    List<VaccinationRecordModel>.from(
        json.decode(str).map((x) => VaccinationRecordModel.fromJson(x)));

String vaccinationRecordModelToJson(List<VaccinationRecordModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class VaccinationRecordModel {
    VaccinationRecordModel({
        this.id,
        this.ah1n1 = true,
        this.hepatitisB = true,
        this.influenza= true,
        this.papilloma = true,
        this.rubella= true,
        this.yellowFever= true,
    });

    int ? id;
    bool ah1n1;
    bool hepatitisB;
    bool influenza;
    bool papilloma;
    bool rubella;
    bool yellowFever;

    factory VaccinationRecordModel.fromJson(Map<String, dynamic> json) => VaccinationRecordModel(
        id: json["_id"],
        ah1n1: json["ah1n1"],
        hepatitisB: json["hepatitisB"],
        influenza: json["influenza"],
        papilloma: json["papilloma"],
        rubella: json["rubella"],
        yellowFever: json["yellowFever"],
    );

    Map<String, dynamic> toJson() => {
        "_id": id,
        "ah1n1": ah1n1,
        "hepatitisB": hepatitisB,
        "influenza": influenza,
        "papilloma": papilloma,
        "rubella": rubella,
        "yellowFever": yellowFever,
    };
}
