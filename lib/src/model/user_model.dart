// To parse this JSON data, do
//
//     final userModel = userModelFromMap(jsonString);

import 'dart:convert';

class UserModel {
  UserModel(
      {this.id,
      this.name = "",
      this.lastName = "",
      this.surName = "",
      this.fullname,
      this.email = "",
      this.documentType,
      this.documentNumber = "",
      this.active,
      this.creationDate,
      this.birthdate = "",
      this.isSpecialist,
      this.isPatient,
      this.address = "",
      this.idDistrict = 1251,
      this.phone = "",
      this.patientDetail,
      this.password});

  String? id;
  String name;
  String lastName;
  String surName;
  String? fullname;
  String email;
  String? documentType;
  String documentNumber;
  bool? active;
  DateTime? creationDate;
  String birthdate;
  bool? isSpecialist;
  bool? isPatient;
  String address;
  int idDistrict;
  String phone;
  PatientDetail? patientDetail;
  String? password;

  factory UserModel.fromJson(String str) => UserModel.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory UserModel.fromMap(Map<String, dynamic> json) => UserModel(
        id: json["_id"],
        name: json["name"],
        lastName: json["lastName"],
        surName: json["surName"],
        fullname: json["fullname"],
        email: json["email"],
        documentType: json["documentType"],
        documentNumber: json["documentNumber"],
        active: json["active"],
        creationDate: DateTime.parse(json["creationDate"]),
        birthdate: json["birthdate"],
        isSpecialist: json["isSpecialist"],
        isPatient: json["isPatient"],
        address: json["address"],
        idDistrict: json["idDistrict"],
        phone: json["phone"],
        patientDetail: PatientDetail.fromMap(json["patientDetail"]),
        password: json["password"],
      );

  Map<String, dynamic> toMap() => {
        "_id": id,
        "name": name,
        "lastName": lastName,
        "surName": surName,
        "fullname": fullname,
        "email": email,
        "documentType": documentType,
        "documentNumber": documentNumber,
        "active": active,
        "creationDate": creationDate?.toIso8601String(),
        "birthdate": birthdate,
        "isSpecialist": isSpecialist,
        "isPatient": isPatient,
        "address": address,
        "idDistrict": idDistrict,
        "phone": phone,
        "patientDetail": patientDetail?.toMap(),
        "password": password,
      };
}

class PatientDetail {
  PatientDetail({
    this.id,
    this.status,
    this.affiliationDate,
    this.hcpbActive,
  });

  String? id;
  String? status;
  DateTime? affiliationDate;
  bool? hcpbActive;

  factory PatientDetail.fromJson(String str) =>
      PatientDetail.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory PatientDetail.fromMap(Map<String, dynamic> json) => PatientDetail(
        id: json["_id"],
        status: json["status"],
        affiliationDate: DateTime.parse(json["affiliationDate"]),
        hcpbActive: json["hcpbActive"],
      );

  Map<String, dynamic> toMap() => {
        "_id": id,
        "status": status,
        "affiliationDate": affiliationDate?.toIso8601String(),
        "hcpbActive": hcpbActive,
      };
}
