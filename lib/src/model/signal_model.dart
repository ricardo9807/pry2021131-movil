// To parse this JSON data, do
//
//     final signalModel = signalModelFromJson(jsonString);

import 'dart:convert';

SignalModel signalModelFromJson(String str) =>
    SignalModel.fromJson(json.decode(str));

String signalModelToJson(SignalModel data) => json.encode(data.toJson());

class SignalModel {
  SignalModel({
    required this.pulse,
    required this.bloodPresure,
    required this.temperature,
    required this.diastolic,
    required this.systolic,
  });

  int pulse;
  int bloodPresure;
  double temperature;
  int diastolic;
  int systolic;

  factory SignalModel.fromJson(Map<String, dynamic> json) => SignalModel(
        pulse: json["pulse"],
        bloodPresure: json["bloodPresure"],
        temperature: json["temperature"].toDouble(),
        diastolic: json["diastolic"],
        systolic: json["systolic"],
      );

  Map<String, dynamic> toJson() => {
        "pulse": pulse,
        "bloodPresure": bloodPresure,
        "temperature": temperature,
        "diastolic": diastolic,
        "systolic": systolic,
      };
}
