import 'dart:convert';

List<FetusesDetailModel> fetusesDetailModelFromJson(String str) =>
    List<FetusesDetailModel>.from(
        json.decode(str).map((x) => FetusesDetailModel.fromJson(x)));

String medicalHistoryModelToJson(List<FetusesDetailModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class FetusesDetailModel {
    FetusesDetailModel({
        this.id,
        this.fcf,
        this.situationFetus = 0,
        this.presentationFetus= 0,
        this.observation,
        this.positionFetus= 0,
    });

    String ? id;
    int? fcf;
    int situationFetus;
    int presentationFetus;
    String? observation;
    int positionFetus;

    factory FetusesDetailModel.fromJson(Map<String, dynamic> json) => FetusesDetailModel(
        id: json["_id"],
        fcf: json["fcf"],
        situationFetus: json["situationFetus"],
        presentationFetus: json["presentationFetus"],
        observation: json["observation"],
        positionFetus: json["positionFetus"],
    );

    Map<String, dynamic> toJson() => {
        "_id": id,
        "fcf": fcf,
        "situationFetus": situationFetus,
        "presentationFetus": presentationFetus,
        "observation": observation,
        "positionFetus": positionFetus,
    };
}
