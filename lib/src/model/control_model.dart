// To parse this JSON data, do
//
//     final controlModel = controlModelFromJson(jsonString);

import 'dart:convert';
import 'package:safeapp/src/model/fetuses_detail_model.dart';

List<ControlModel> controlModelFromJson(String str) => List<ControlModel>.from(
    json.decode(str).map((x) => ControlModel.fromJson(x)));

String controlModelToJson(List<ControlModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ControlModel {
  ControlModel({
    this.id = "",
    this.dateAttention,
    this.actualWeight = 0,
    this.weightGain = 0,
    this.pulse = 0,
    this.imc = 0.0,
    this.nextControlDate,
    this.temperature = 0.0,
    this.systolicPressure = 0,
    this.diastolicPressure = 0,
    this.uterineHeight = 0,
    this.breathingFrequency = 0,
    this.systemDiagnosis = "",
    this.finalDiagnosis = "",
    this.numberControl = 0,
    this.active = false,
    this.fetusesDetail,
    this.recommendations = "",
    this.idSpecialist = "",
    this.nameSpecialist = "",
    this.idHcpb = "",
  });

  String id;
  String? dateAttention;
  int actualWeight;
  int weightGain;
  int pulse;
  double imc;
  String? nextControlDate;
  double temperature;
  int systolicPressure;
  int diastolicPressure;
  int uterineHeight;
  int breathingFrequency;
  String systemDiagnosis;
  String finalDiagnosis;
  int numberControl;
  bool active;
  List<FetusesDetailModel>? fetusesDetail;
  String? recommendations;
  String? idSpecialist;
  String? nameSpecialist;
  String? idHcpb;

  factory ControlModel.fromJson(Map<String, dynamic> json) => ControlModel(
        id: json["_id"],
        dateAttention: (json["dateAttention"]),
        actualWeight: json["actualWeight"],
        weightGain: json["weightGain"],
        pulse: json["pulse"],
        imc: json["imc"].toDouble(),
        nextControlDate: (json["nextControlDate"]),
        temperature: json["temperature"].toDouble(),
        systolicPressure: json["systolicPressure"],
        diastolicPressure: json["diastolicPressure"],
        uterineHeight: json["uterineHeight"],
        breathingFrequency: json["breathingFrequency"],
        systemDiagnosis: json["systemDiagnosis"],
        finalDiagnosis: json["finalDiagnosis"],
        numberControl: json["numberControl"],
        active: json["active"],
        recommendations: json["recommendations"],
        idSpecialist: json["idSpecialist"],
        nameSpecialist: json["nameSpecialist"],
        idHcpb: json["idHCPB"],
        fetusesDetail: List<FetusesDetailModel>.from(
            json["fetusesDetail"].map((x) => FetusesDetailModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "dateAttention": dateAttention,
        "actualWeight": actualWeight,
        "weightGain": weightGain,
        "pulse": pulse,
        "imc": imc,
        "nextControlDate": nextControlDate,
        "temperature": temperature,
        "systolicPressure": systolicPressure,
        "diastolicPressure": diastolicPressure,
        "uterineHeight": uterineHeight,
        "breathingFrequency": breathingFrequency,
        "systemDiagnosis": systemDiagnosis,
        "finalDiagnosis": finalDiagnosis,
        "numberControl": numberControl,
        "active": active,
        "recommendations": recommendations,
        "idSpecialist": idSpecialist,
        "nameSpecialist": nameSpecialist,
        "idHCPB": idHcpb,
      };
}
