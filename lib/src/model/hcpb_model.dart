// To parse this JSON data, do
//
//     final hcpbModel = hcpbModelFromMap(jsonString);

import 'dart:convert';

import 'package:safeapp/src/model/medical_history_model.dart';
import 'package:safeapp/src/model/vaccination_record_model.dart';

List<HcpbModel> hcpbModelFromJson(String str) =>
    List<HcpbModel>.from(json.decode(str).map((x) => HcpbModel.fromJson(x)));

String hcpbModelToJson(List<HcpbModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class HcpbModel {
  HcpbModel({
    this.id = "",
    this.creationDate = "",
    this.ageMenarche = 0,
    this.ageFirstSexualExperience = 0,
    this.lastContraceptiveMethod = "",
    this.lastDateMenstruation = "",
    this.pregnantHeight = 0,
    this.pregnantWeight = 0,
    this.probableDateBirth,
    this.gestationalAge = 0,
    this.currentControl = 0,
    this.status = "",
    this.nextControlDate = "",
    this.multiplePregnancy = 0,
    this.highStressLevel = 0,
    this.previousPretermPregnancy = 0,
    this.shortUterineNeck = 0,
    this.medicalHistory,
    //this.vaccinationRecord,
  });

  String? id;
  String? creationDate;
  int? ageMenarche;
  int? ageFirstSexualExperience;
  String lastContraceptiveMethod;
  String? lastDateMenstruation;
  int pregnantHeight;
  int pregnantWeight;
  String? probableDateBirth;
  int gestationalAge;
  int currentControl;
  String status;
  String? nextControlDate;
  int multiplePregnancy;
  int highStressLevel;
  int previousPretermPregnancy;
  int shortUterineNeck;
  List<MedicalHistoryModel>? medicalHistory;
  //VaccinationRecordModel? vaccinationRecord;

  factory HcpbModel.fromJson(Map<String, dynamic> json) => HcpbModel(
        id: json["_id"],
        creationDate: (json["creationDate"]),
        ageMenarche: json["ageMenarche"],
        ageFirstSexualExperience: json["ageFirstSexualExperience"],
        lastContraceptiveMethod: json["lastContraceptiveMethod"],
        lastDateMenstruation: (json["lastDateMenstruation"]),
        pregnantHeight: json["pregnantHeight"],
        pregnantWeight: json["pregnantWeight"],
        probableDateBirth: (json["probableDateBirth"]),
        gestationalAge: json["gestationalAge"],
        currentControl: json["currentControl"],
        status: json["status"],
        nextControlDate: (json["nextControlDate"]),
        multiplePregnancy: json["multiplePregnancy"],
        highStressLevel: json["highStressLevel"],
        previousPretermPregnancy: json["previousPretermPregnancy"],
        shortUterineNeck: json["shortUterineNeck"],
        medicalHistory: List<MedicalHistoryModel>.from(
            json["medicalHistory"].map((x) => MedicalHistoryModel.fromJson(x))),
        //vaccinationRecord: VaccinationRecordModel.from(json["vaccinationRecord"].map((x) => VaccinationRecordModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "creationDate": creationDate,
        "ageMenarche": ageMenarche,
        "ageFirstSexualExperience": ageFirstSexualExperience,
        "lastContraceptiveMethod": lastContraceptiveMethod,
        "lastDateMenstruation": lastDateMenstruation,
        "pregnantHeight": pregnantHeight,
        "pregnantWeight": pregnantWeight,
        "probableDateBirth": probableDateBirth,
        "gestationalAge": gestationalAge,
        "currentControl": currentControl,
        "status": status,
        "nextControlDate": nextControlDate,
        "multiplePregnancy": multiplePregnancy,
        "highStressLevel": highStressLevel,
        "previousPretermPregnancy": previousPretermPregnancy,
        "shortUterineNeck": shortUterineNeck,
      };
}
