import 'dart:convert';

import 'package:safeapp/src/model/fetuses_detail_model.dart';

List<EmergencyModel> emergencyModelFromJson(String str) =>
    List<EmergencyModel>.from(json.decode(str).map((x) => EmergencyModel.fromJson(x)));

String emergencyModelToJson(List<EmergencyModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class EmergencyModel {
  EmergencyModel({
    this.id="",
    this.dateAttention,
    this.clinic="",
    this.initialObservation="",
    this.lastControlDate,
    this.gestationalAge=0,
    this.systolicPressure=0,
    this.diastolicPressure=0,
    this.temperature=0,
    this.pulse=0,
    this.breathingFrequency= 0,
    this.systemDiagnosis="",
    this.specialistName="",
    this.colegiatureNumber="",
    this.finalDiagnosis="",
    this.recommendations="",
    this.interPregnant=0,
    this.terminatePregnancy=0,
    this.pretermBirth=0,
    this.active=true,
    this.fetusesDetail,
    this.idPatient="",
    this.idHcpb="",
  });

    String id;
    String? dateAttention;
    String clinic;
    String initialObservation;
    String? lastControlDate;
    int gestationalAge;
    int systolicPressure;
    int diastolicPressure;
    int temperature;
    int pulse;
    int breathingFrequency;
    String systemDiagnosis;
    String specialistName;
    String colegiatureNumber;
    String finalDiagnosis;
    String recommendations;
    int interPregnant;
    int terminatePregnancy;
    int pretermBirth;
    bool active;
    List<FetusesDetailModel>? fetusesDetail;
    String idPatient;
    String idHcpb;

  factory EmergencyModel.fromJson(Map<String, dynamic> json) => EmergencyModel(
        id: json["_id"],
        dateAttention:(json["dateAttention"]),
        clinic: json["clinic"],
        initialObservation: json["initialObservation"],
        lastControlDate: (json["lastControlDate"]),
        gestationalAge: json["gestationalAge"],
        systolicPressure: json["systolicPressure"],
        diastolicPressure: json["diastolicPressure"],
        temperature: json["temperature"],
        pulse: json["pulse"],
        breathingFrequency: json["breathingFrequency"],
        systemDiagnosis: json["systemDiagnosis"],
        specialistName: json["specialistName"],
        colegiatureNumber: json["colegiatureNumber"],
        finalDiagnosis: json["finalDiagnosis"],
        recommendations: json["recommendations"],
        interPregnant: json["interPregnant"],
        terminatePregnancy: json["terminatePregnancy"],
        pretermBirth: json["pretermBirth"],
        active: json["active"],
        fetusesDetail: List<FetusesDetailModel>.from(json["fetusesDetail"].map((x) => FetusesDetailModel.fromJson(x))),
        idPatient: json["idPatient"],
        idHcpb: json["idHCPB"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "dateAttention": dateAttention,
        "clinic": clinic,
        "initialObservation": initialObservation,
        "lastControlDate": lastControlDate,
        "gestationalAge": gestationalAge,
        "systolicPressure": systolicPressure,
        "diastolicPressure": diastolicPressure,
        "temperature": temperature,
        "pulse": pulse,
        "breathingFrequency": breathingFrequency,
        "systemDiagnosis": systemDiagnosis,
        "specialistName": specialistName,
        "colegiatureNumber": colegiatureNumber,
        "finalDiagnosis": finalDiagnosis,
        "recommendations": recommendations,
        "interPregnant": interPregnant,
        "terminatePregnancy": terminatePregnancy,
        "pretermBirth": pretermBirth,
        "active": active,
        "idPatient": idPatient,
        "idHCPB": idHcpb,
      };
}
