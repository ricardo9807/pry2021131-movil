// To parse this JSON data, do
//
//     final usuarioModel = usuarioModelFromJson(jsonString);

import 'dart:convert';

UsuarioModel usuarioModelFromJson(String str) =>
    UsuarioModel.fromJson(json.decode(str));

String usuarioModelToJson(UsuarioModel data) => json.encode(data.toJson());

class UsuarioModel {
  UsuarioModel({
    required this.id,
    required this.name,
    required this.firstLastName,
    required this.secondLastName,
    required this.dni,
    required this.birth,
    required this.phone,
    required this.email,
  });

  String id;
  String name;
  DateTime firstLastName;
  String secondLastName;
  String dni;
  String birth;
  String phone;
  String email;

  factory UsuarioModel.fromJson(Map<String, dynamic> json) => UsuarioModel(
        id: json["id"],
        name: json["name"],
        firstLastName: DateTime.parse(json["firstLastName"]),
        secondLastName: json["secondLastName"],
        dni: json["dni"],
        birth: json["birth"],
        phone: json["phone"],
        email: json["email"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "firstLastName": firstLastName.toIso8601String(),
        "secondLastName": secondLastName,
        "dni": dni,
        "birth": birth,
        "phone": phone,
        "email": email,
      };
}
