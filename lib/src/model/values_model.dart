// To parse this JSON data, do
//
//     final valuesModel = valuesModelFromJson(jsonString);

import 'dart:convert';

import 'package:flutter/cupertino.dart';

ValuesModel valuesModelFromJson(String str) =>
    ValuesModel.fromJson(json.decode(str));

String valuesModelToJson(ValuesModel data) => json.encode(data.toJson());

class ValuesModel {
  ValuesModel({
    this.specialist,
    this.age,
    this.currentGestationalAge,
    this.nextControl,
    this.classification,
    this.actualWeight,
    this.multiplePregnancy,
    this.previousPretermPregnancy,
    this.pregnantHeight,
    this.name,
    this.email,
    this.phone,
    this.documentNumber,
  });

  Specialist? specialist;
  int? age;
  int? currentGestationalAge;
  String? nextControl;
  String? classification;
  int? actualWeight;
  String? multiplePregnancy;
  String? previousPretermPregnancy;
  int? pregnantHeight;
  String? name;
  String? email;
  String? phone;
  String? documentNumber;

  factory ValuesModel.fromJson(Map<String, dynamic> json) => ValuesModel(
        specialist: Specialist.fromJson(json["specialist"]),
        age: json["age"],
        currentGestationalAge: json["currentGestationalAge"],
        nextControl: json["nextControl"],
        classification: json["classification"],
        actualWeight: json["actualWeight"],
        multiplePregnancy: json["multiplePregnancy"],
        previousPretermPregnancy: json["previousPretermPregnancy"],
        pregnantHeight: json["pregnantHeight"],
        name: json["name"],
        email: json["email"],
        phone: json["phone"],
        documentNumber: json["documentNumber"],
      );

  Map<String, dynamic> toJson() => {
        "specialist": specialist!.toJson(),
        "age": age,
        "currentGestationalAge": currentGestationalAge,
        "nextControl": nextControl,
        "classification": classification,
        "actualWeight": actualWeight,
        "multiplePregnancy": multiplePregnancy,
        "previousPretermPregnancy": previousPretermPregnancy,
        "pregnantHeight": pregnantHeight,
        "name": name,
        "email": email,
        "phone": phone,
        "documentNumber": documentNumber,
      };
}

class Specialist {
  Specialist({
    this.name = "",
    this.documentNumber = "",
    this.colegiatureNumber = "",
    this.phone = "",
  });

  @required
  String name;
  @required
  String documentNumber;
  @required
  String colegiatureNumber;
  @required
  String phone;

  factory Specialist.fromJson(Map<String, dynamic> json) => Specialist(
        name: json["name"],
        documentNumber: json["documentNumber"],
        colegiatureNumber: json["colegiatureNumber"],
        phone: json["phone"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "documentNumber": documentNumber,
        "colegiatureNumber": colegiatureNumber,
        "phone": phone,
      };
}
