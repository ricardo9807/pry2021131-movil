import 'dart:convert';

List<MedicalHistoryModel> medicalHistoryModelFromJson(String str) =>
    List<MedicalHistoryModel>.from(
        json.decode(str).map((x) => MedicalHistoryModel.fromJson(x)));

String medicalHistoryModelToJson(List<MedicalHistoryModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class MedicalHistoryModel {
  MedicalHistoryModel({
    required this.id,
    required this.description,
    required this.observation,
    required this.diagnosisDate,
  });

  String id;
  String description;
  String observation;
  String diagnosisDate;

  factory MedicalHistoryModel.fromJson(Map<String, dynamic> json) =>
      MedicalHistoryModel(
        id: json["_id"],
        description: json["description"],
        observation: json["observation"],
        diagnosisDate: (json["diagnosisDate"]),
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "description": description,
        "observation": observation,
        "diagnosisDate": diagnosisDate,
      };
}
