import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:safeapp/src/preferences/preferencias_usuario.dart';
import 'package:safeapp/src/providers/user_provider.dart';
import 'package:safeapp/src/routes/routes.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final preferencia = new PreferenciasUsuario();
  await preferencia.initPreferencias();

  runApp(AppState());
}

class AppState extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => UserProvider(), lazy: false),
      ],
      child: MyApp(),
    );
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      initialRoute: 'loading',
      routes: appRoutes,
    );
  }
}
